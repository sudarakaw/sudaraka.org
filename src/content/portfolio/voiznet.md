+++
title = "VoizNet"
description = "VoizNet - Voip service provider web site developed on the GlobalNet VoIP Billing Platform."
date = 2005-04-15T05:53:47+05:30

[extra]
keywords = "voiznet,web,voip,end user,billing,php,mysql,javascript,html,linkpoint,first data,xml"
css = [ "project" ]
+++


![](/assets/img/portfolio/voiznet.jpg)

Summary
-|-
Url  | http://www.voiznet.com/
Released  | 2005
Status  | Off-line<br> (different version on-line)
Technologies  | JavaScript, LAMP, XML
3<sup>rd</sup> Party Integrations  | [LinkPoint](http://www.firstdata.com/linkpoint/)

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>

End user VoIP solution build on [GlobalNet VoIP Billing
Platform](/portfolio/globalnet-voip-billing-platform).
