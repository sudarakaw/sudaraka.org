+++
title = "iDial IP"
description = "iDial IP - End user VoIP solution build on GlobalNet VoIP Billing Platform."
date = 2005-01-15T05:01:08+05:30

[extra]
keywords = "idial ip,web,voip,end user,billing,service,php,mysql,javascript,css,html"
css = [ "project" ]
+++


![](/assets/img/portfolio/idial-ip.jpg)

Summary
-|-
Url  | http://www.idialip.com/
Released  | 2005
Status  | Off-line
Technologies  | CSS, JavaScript, LAMP, XML

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>

End user VoIP solution build on [GlobalNet VoIP Billing Platform](/portfolio/globalnet-voip-billing-platform).

Web site was developed for GlobalNet to demonstrate the full features of API
system of the [GlobalNet VoIP Billing Platform](/portfolio/globalnet-voip-billing-platform),
while it also served it's own customer base.

This web site was awarded the **"Best of Show"** award at
[Internet Telephony Conference & Expo](http://www.tmcnet.com/voip/conference/m05/) 2005 & 2006.

![](/assets/img/portfolio/idial-ip-2005.gif)

![](/assets/img/portfolio/idial-ip-2006.gif)

<div class="clear"></div>
