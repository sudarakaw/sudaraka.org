+++
title = "No Deposit Properties"
description = "Real-estate/property listing service."
date = 2005-08-15T01:12:01+05:30

[extra]
keywords = "web,project,php,css,html"
css = [ "project" ]
+++


![](/assets/img/portfolio/no-deposit-properties.jpg)

Summary
-|-
Url  | http://www.nodepositproperties.com/
Released  | 2005
Status  | Off-line
Technologies  | CSS, LAMP

Role of Sudaraka
-|-
Database Designer<br> Graphics Designer<br> Programmer

<div class="clear"></div>

Real-estate/property listing web site with back-end administration interface to
maintain the listed property items.
