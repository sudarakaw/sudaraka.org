+++
title = "iUSA"
description = "iUSA - Complete bilingual Voip solution developed on the GlobalNet VoIP Billing Platform."
date = 2005-05-15T12:00:00+05:30

[extra]
keywords = "iusa,web,voip,end user,,billing,php,mysql,javascript,css,html"
css = [ "project" ]
+++


![](/assets/img/portfolio/iusa.jpg)

Summary
-|-
Url  | http://www.iusa.com/
Released  | 2005
Status  | Off-line
Technologies  | CSS, JavaScript, LAMP, XML
3<sup>rd</sup> Party Integrations  | [LinkPoint](http://www.firstdata.com/linkpoint/)

Role of Sudaraka
-|-
Programmer<br> System Designer

<div class="clear"></div>

Full featured web solution based on [GlobalNet VoIP Billing Platform](/portfolio/globalnet-voip-billing-platform),
including hardware provisioning capabilities.

This web site was developed with bilingual (English & Spanish) content
provided under same domain depending on the users choice.
