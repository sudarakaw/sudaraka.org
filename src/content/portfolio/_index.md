+++
title = "Portfolio"
description = "List of projects I have been involved with in my career."

[extra]
keywords = "projects, sudaraka wijesinghe, experience"
css = [ "portfolio" ]
no_index = true
+++


_My bread & butter - list of project from my freelance & entrepreneurial
career that contributed to me experience since 2004._

<div class="project-list">

{{ portfolio_project(title="Sync POS and Online Store", link="pos-web-db-sync-johnandpetes", image="pos-web-db-sync-johnandpetes.jpg") }}

{{ portfolio_project(title="OpenSprinkler Pi - Monitor", link="opensprinkler-pi-monitor", image="opensprinkler-pi-monitor.jpg") }}

{{ portfolio_project(title="CBY Mens Club Softball League", link="cbymcsl", image="cbymcsl-small.jpg") }}

{{ portfolio_project(title="EskimoAutoAir.Com", link="eskimoautoair", image="eskimoautoair.jpg") }}

{{ portfolio_project(title="Watch Repair Co.", link="watch-repair", image="watch-repair.jpg") }}

{{ portfolio_project(title="CTS Athletics", link="cts-athletics", image="cts-athletics.jpg") }}

{{ portfolio_project(title="MIAMICA", link="miamica", image="miamica.jpg") }}

{{ portfolio_project(title="The American Jewish Youth Forum", link="the-american-jewish-youth-forum", image="the-american-jewish-youth-forum.jpg") }}

{{ portfolio_project(title="Luxury Exhibit", link="luxury-exhibit", image="luxury-exhibit.jpg") }}

{{ portfolio_project(title="Lost Tribes Brew", link="lost-tribes-brew", image="lost-tribes-brew.jpg") }}

{{ portfolio_project(title="K T Cunningham", link="k-t-cunningham", image="k-t-cunningham.jpg") }}

{{ portfolio_project(title="Caribbean Mortgage Specialist", link="caribbean-mortgage-specialist", image="caribbean-mortgage-specialist.jpg") }}

{{ portfolio_project(title="Bridal Albums", link="bridal-albums", image="bridal-albums.jpg") }}

{{ portfolio_project(title="J-LAB Technology, Inc", link="j-lab-technology", image="j-lab-technology.jpg") }}

{{ portfolio_project(title="bhayward.com", link="bhayward-com", image="bhayward-com.jpg") }}

{{ portfolio_project(title="Simcha Day Camp", link="simcha-day-camp", image="simcha-day-camp.jpg") }}

{{ portfolio_project(title="Johnson Motors", link="johnson-motors", image="johnson-motors.jpg") }}

{{ portfolio_project(title="Sports Resorts Development", link="sports-resorts-development", image="sports-resorts-development.jpg") }}

{{ portfolio_project(title="Dealtwist", link="dealtwist", image="dealtwist.jpg") }}

{{ portfolio_project(title="MyOwnOnlineResume", link="myownonlineresume", image="myownonlineresume.jpg") }}

{{ portfolio_project(title="Mission Bay Shul", link="mission-bay-shul", image="mission-bay-shul.jpg") }}

{{ portfolio_project(title="Komarov Inc.", link="komarov-inc", image="komarov-inc.jpg") }}

{{ portfolio_project(title="Hamwoods Group", link="hamwoods-group", image="hamwoods-group.jpg") }}

{{ portfolio_project(title="CommonThreadz", link="commonthreadz", image="commonthreadz.jpg") }}

{{ portfolio_project(title="Pasadena Bar Association", link="pasadena-bar-association", image="pasadena-bar-association.jpg") }}

{{ portfolio_project(title="310 Artists", link="310-artists", image="310-artists.jpg") }}

{{ portfolio_project(title="VoipPAL", link="voippal", image="voippal.jpg") }}

{{ portfolio_project(title="RadiatorsWarehouse.Com", link="radiatorswarehouse-com", image="radiatorswarehouse-com.jpg") }}

{{ portfolio_project(title="Dynamic Feild Search", link="dynamic-feild-search", image="dynamic-feild-search.jpg") }}

{{ portfolio_project(title="E-Networks CRM", link="e-networks-crm", image="e-networks-crm.jpg") }}

{{ portfolio_project(title="iDial Direct", link="idial-direct", image="idial-direct.jpg") }}

{{ portfolio_project(title="No Deposit Properties", link="no-deposit-properties", image="no-deposit-properties.jpg") }}

{{ portfolio_project(title="Picture Vehicles", link="picture-vehicles", image="picture-vehicles.jpg") }}

{{ portfolio_project(title="iUSA", link="iusa", image="iusa.jpg") }}

{{ portfolio_project(title="VoizNet", link="voiznet", image="voiznet.jpg") }}

{{ portfolio_project(title="LookBookFly.com", link="lookbookfly-com", image="lookbookfly-com.jpg") }}

{{ portfolio_project(title="iDial IP", link="idial-ip", image="idial-ip.jpg") }}

{{ portfolio_project(title="Caribbean360", link="caribbean360", image="caribbean360.jpg") }}

{{ portfolio_project(title="GlobalNet VoIP Billing Platform", link="globalnet-voip-billing-platform", image="globalnet-voip-billing-platform.jpg") }}

{{ portfolio_project(title="International Dial Direct", link="international-dial-direct", image="international-dial-direct.jpg") }}

{{ portfolio_project(title="SuAyuda", link="suayuda", image="suayuda.jpg") }}

</div>
