+++
title = "Mission Bay Shul"
description = "Mission Bay Shul (Boca Torah) - web site for a torah center."
date = 2009-02-15T23:32:57+05:30

[extra]
keywords = "mission bay shul,boca torah,web,php,html,javascript,css,smarty"
css = [ "project" ]
+++


![](/assets/img/portfolio/mission-bay-shul.jpg)

Summary
-|-
Url  | http://bocatorah.com/
Released  | 2009
Status  | On-line
Technologies  | CSS, JavaScript, LAMP, Smarty

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>
