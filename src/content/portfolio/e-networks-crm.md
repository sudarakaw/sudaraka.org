+++
title = "E-Networks CRM "
description = "E-Networks Customer Relations Management System - maintain customer, reseller agent services and product transaction and day-to-day transaction and provisioning records."
date = 2006-07-15T04:53:00+05:30

[extra]
keywords = "e-networks,crm,web,billing,job provision,service,php,mysql,javascript,html,smarty,web framework"
css = [ "project" ]
+++


![](/assets/img/portfolio/e-networks-crm.jpg)

Summary
-|-
Development Releases  | 2006, 2010
Status  | Off-line
Technologies  | [FPDF](http://www.fpdf.org/), JavaScript, LAMP

Role of Sudaraka
-|-
Database Designer<br> Programmer<br> System Designer

<div class="clear"></div>

E-Network Customer Relations Management System is a web based service to
maintain customer, reseller agent services and product transaction and also to
keep records of  day-to-day transaction and provisioning.


**First Phase - 2006**


Originally designed to be a simple consumer web site for the
[GlobalNet VoIP Billing Platform](/assets/portfolio/globalnet-voip-billing-platform/),
and eventually developed in to a CRM. Due to some unfortunate natural disaster
faced by the company, the project was postponed.

**Second Phase - 2010**


Project was revived and re-coded using the Sudaraka's experimental web
framework, and since GlobalNet work services were not available at the time
plan was to use another RADIUS service for the telecommunication module.

Eventually the company lost interest in the project and it remains dormant.

## Features

  * Commission based reseller agent system.
  * Service & product maintenance.
  * Work order schedule system.
  * Consumer maintenance.
  * Automated monthly billing.
  * VoIP module integrated to [GlobalNet VoIP Billing Platform](/assets/img/portfolio/globalnet-voip-billing-platform/).
  * Multi-currency support.

