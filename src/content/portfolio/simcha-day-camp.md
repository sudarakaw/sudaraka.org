+++
title = "Simcha Day Camp"
description = "Simcha Day Camp - official web site for a Jewish summer camp for children."
date = 2009-11-15T01:53:20+05:30

[extra]
keywords = "Simcha Day Camp,web,summer camp,php,web framework,mysql,jquery,html,css"
css = [ "project" ]
+++


![](/assets/img/portfolio/simcha-day-camp.jpg)

Summary
-|-
Url  | http://www.simchadaycamp.com/
Released  | 2009
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Smarty

Role of Sudaraka
-|-
Database Designer<br> Graphics Designer<br> Programmer<br> System Designer

<div class="clear"></div>

Official web site for a Jewish summer camp for children.

This is the first production application built using Sudaraka's experimental
web framework, as of today it runs on a upgraded framework that is maintained
along with the web application it self.
