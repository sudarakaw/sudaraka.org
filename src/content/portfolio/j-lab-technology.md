+++
title = "J-LAB Technology, Inc"
description = "J-LAB Technology, Inc - corporate web site for an EDI Consulting Service."
date = 2010-01-15T03:06:06+05:30

[extra]
keywords = "J-LAB Technology, Inc,web,corporate,php,mysql,jquery,html,css,smarty"
css = [ "project" ]
+++


![](/assets/img/portfolio/j-lab-technology.jpg)

Summary
-|-
Url  | http://www.jlabtech.com/
Released  | 2010
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Smarty

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>

Corporate web site for an EDI Consulting Service.

