+++
title = "SuAyuda"
description = "Automated system for matching consumer needs with service providers."
date = 2004-03-15T12:20:45+05:30

[extra]
keywords = "suayuda,web,automated,service provider,consumer,asp,visual basic,activex,ms-sql,linkpoint,first data,xml"
css = [ "project" ]
+++


![](/assets/img/portfolio/suayuda.jpg)

Summary
-|-
Url  | http://www.suayuda.com/
Released  | 2003
Status  | Off-line
Technologies  | ASP, COM/ActiveX, MS-SQL, Visual Basic
3<sup>rd</sup> Party Integrations  | [LinkPoint](http://www.firstdata.com/linkpoint/)

Role of Sudaraka
-|-
Database Designer<br> Programmer<br> System Designer

<div class="clear"></div>

Automated system that match the needs of consumers with registered service
providers. Web based user interface was implemented using ASP, while the
back-end models and automation was programmed using Visual Basic with
COM/ActiveX technology.


## Features

  * Automated requirement - service matching
  * 3-way match validation (Request, Accept, Acknowledge)
  * Identifications and alert generation for unmatched request.
