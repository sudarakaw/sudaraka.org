+++
title = "Sports Resorts Development"
description = "Sports Resorts Development - corporate web site for a real estate development company."
date = 2009-05-15T16:26:05+05:30

[extra]
keywords = "Sports Resorts Development,web,corporate,php,jquery,css,html,flash"
css = [ "project" ]
+++


![](/assets/img/portfolio/sports-resorts-development.jpg)

Summary
-|-
Url  | http://sportsresortsdevelopment.com/
Released  | 2009
Status  | On-line
Technologies  | CSS, Flash, jQuery, PHP, Smarty

Role of Sudaraka
-|-
Hosting Service Provider<br> Programmer

<div class="clear"></div>

Corporate web site for a real estate development company.

