+++
title = "310 Artists"
description = "310 Artists - web site for group of artists to showcase their creative work."
date = 2008-09-15T05:45:00+05:30

[extra]
keywords = "310 artists,web,art,gallery,php,mysql,html,css,smarty"
css = [ "project" ]
+++


![](/assets/img/portfolio/310-artists.jpg)

Summary
-|-|-
Url  | http://310artists.com/
Released  | 2007
Status  | On-line<br> (with a modified interface)
Technologies  | CSS, JavaScript, LAMP, Smarty

&nbsp;

Role of Sudaraka
-|-
Database Designer<br> Programmer<br> System Designer

<div class="clear"></div>

Web site for group of artists that enables them to upload ans showcase their creative work.

