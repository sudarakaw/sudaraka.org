+++
title = "Sync POS and Online Store"
description = "Sync POS and Online Store - JohnAndPetes.Com - Two sided synchronization application between SQL Server (POS) and MySQL (Online Store) databases over HTTP."
date = 2013-09-12T12:01:24+05:30

[extra]
keywords = "johnandpetes,database,synchonize,pos,web,python,php,sql server,mysql,http"
css = [ "project" ]
+++


![](/assets/img/portfolio/pos-web-db-sync-johnandpetes.jpg)

Summary
-|-
Released  | 2013 - March
Status  | On-line
Technologies  | Python, LAMP, SQL Server

Role of Sudaraka
-|-
Programmer<br> System Designer

<div class="clear"></div>

A two sided database synchronization application between SQL Server (POS) and
MySQL (Online Store) databases over HTTP.

