+++
title = "bhayward.com"
description = "bhayward.com - personal biography web site for a Film Director."
date = 2009-12-15T02:29:00+05:30

[extra]
keywords = "bhayward.com,web,personal,biography,php,html,css"
css = [ "project" ]
+++


![](/assets/img/portfolio/bhayward-com.jpg)

Summary
-|-
Url  | http://bhayward.com/
Released  | 2009
Status  | Off-line<br>(Different version on-line)
Technologies  | CSS, LAMP, Smarty

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>

Personal biography web site for a Film Director.

