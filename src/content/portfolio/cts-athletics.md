+++
title = "CTS Athletics"
description = "CTS Athletics - athletic organization in the NYC running basketball leagues for adult males."
date = 2011-11-15T10:00:00+05:30

[extra]
keywords = "CTS Athletics,web,basketball league,php,web framework,mysql,jquery,html,css"
css = [ "project" ]
+++


![](/assets/img/portfolio/cts-athletics.jpg)

Summary
-|-
Url  | http://ctsathletics.com/
Released  | 2011
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Smarty

Role of Sudaraka
-|-
Database Designer<br> Programmer<br> System Designer

<div class="clear"></div>

Official Web site for an athletic organization in the NYC, running basketball
leagues for adult males.
