+++
title = "Caribbean360"
description = "Web site for promoting tourism, real-estate and social interaction in the Caribbean region."
date = 2004-08-15T20:12:00+05:30

[extra]
keywords = "web,project,php,mysql,javascript,html"
css = [ "project" ]
+++


![](/assets/img/portfolio/caribbean360.jpg)

Summary
-|-
Url  | http://caribbean360.net
Released  | 2004
Status  | Off-line
Technologies  | LAMP

&nbsp;

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>

Web site for promoting tourism, real-estate and social interaction in the Caribbean region.
