+++
title = "iDial Direct"
description = "iDial Direct - End user VoIP solution build on GlobalNet VoIP Billing Platform."
date = 2005-08-15T18:55:16+05:30

[extra]
keywords = "idial direct,web,voip,end user,billing,service,php,mysql,javascript,css,html,linkpoint,first data"
css = [ "project" ]
+++


![](/assets/img/portfolio/idial-direct.jpg)

Summary
-|-
Url  | http://www.idialdirect.com/
Released  | 2005
Status  | Off-line
Technologies  | CSS, JavaScript, LAMP, XML
3<sup>rd</sup> Party Integrations  | [LinkPoint](http://www.firstdata.com/linkpoint/)

Role of Sudaraka
-|-
Graphics Designer<br> Programmer

<div class="clear"></div>

End user VoIP solution build on [GlobalNet VoIP Billing Platform](/portfolio/globalnet-voip-billing-platform).
