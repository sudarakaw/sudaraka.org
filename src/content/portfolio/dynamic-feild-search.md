+++
title = "Dynamic Feild Search"
description = "Dynamic Field Search - a web application to make possible for dynamically define fields of a record and search them"
date = 2007-04-15T07:10:00+05:30

[extra]
keywords = "dynamic field search,web,search,dynamic,fields,php,mysql,javascript,html"
css = [ "project" ]
+++


![](/assets/img/portfolio/dynamic-feild-search.jpg)

Summary
-|-
Released  | 2007
Status  | Off-line
Technologies  | CSS, JavaScript, LAMP

Role of Sudaraka
-|-
Database Designer<br> Graphics Designer<br> Programmer

<div class="clear"></div>

A web application to make possible for dynamically define fields of a record and
search them.
