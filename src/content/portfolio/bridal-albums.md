+++
title = "Bridal Albums"
description = "Bridal Albums - web service for a wedding photography service and their customers."
date = 2008-09-15T04:11:00+05:30

[extra]
keywords = "Bridal Albums,web,photocart,wordpress,php,mysql,jquery,html,css,smarty"
css = [ "project" ]
+++


![](/assets/img/portfolio/bridal-albums.jpg)

Summary
-|-
Url  | http://www.bridalalbums.net/
Released  | 2010
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Smarty, Wordpress
3<sup>rd</sup> Party Integrations  | [PhotoCart](http://www.picturespro.com/i/photo_cart/)

&nbsp;

Role of Sudaraka
-|-
Programmer |

<div class="clear"></div>

A web site for a wedding photography service that allows their customers to upload photos and various prints based on them.

Application consists of 3 sections built on:

  * [PhotoCart](http://www.picturespro.com/i/photo_cart/), a web based application that is photographers to upload their work and allow customers to purchase prints of them. PhotoCart application was modified for the Bridal Albums to allow the customers to upload their own photos and request prints from those.
  * Blog for the service was implemented using [Wordpress](http://wordpress.org/).
  * Search too for the users to locate photographers in their area was implemented using the Sudaraka's experimental web framework.

