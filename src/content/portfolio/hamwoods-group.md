+++
title = "Hamwoods Group"
description = "Hamwoods Group - corporate web site for a portfolio building and management consultancy firm."
date = 2008-10-15T22:19:45+05:30

[extra]
keywords = "Hamwoods Group,web,corporate,management firm,php,jquery,html,css,smarty"
css = [ "project" ]
+++


![](/assets/img/portfolio/hamwoods-group.jpg)

Summary
-|-
Url  | http://hamwoods.com/
Released  | 2006
Second Release  | 2008
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Smarty

Role of Sudaraka
-|-
Graphics Designer<br> Hosting Service Provider<br> Programmer

![](/assets/img/portfolio/hamwoods-group-2.jpg)

<div class="clear"></div>

Corporate web site for a portfolio building and management consultancy firm.
