+++
title = "MyOwnOnlineResume"
description = "MyOwnOnlineResume - a web site that users can signup for space to host their resume."
date = 2009-03-15T19:25:56+05:30

[extra]
keywords = "MyOwnOnlineResume,web,resume,php,mysql,jquery,html,css,smarty"
css = [ "project" ]
+++


![](/assets/img/portfolio/myownonlineresume.jpg)

Summary
-|-
Url  | http://www.myownonlineresume.com/
Released  | 2009
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Smarty

Role of Sudaraka
-|-
Database Designer<br> Programmer

![](/assets/img/portfolio/myownonlineresume-2.jpg)

<div class="clear"></div>
