+++
title = "GlobalNet VoIP Billing Platform"
description = "Web based billing platform for a VoIP communication system."
date = 2004-06-15T09:49:31+05:30

[extra]
keywords = "web,project,billing,php,mysql,oracle,javascript,css,html,asterisk,linkpoint,first data,oracle,xml"
css = [ "project" ]
+++


![](/assets/img/portfolio/globalnet-voip-billing-platform.jpg)

Summary
-|-
Url  | http://mis.gbne.net/
Url  | http://amis.gbne.net/
Released  | 2004
Status  | Off-line
Technologies  | [Asterisk](http://www.asterisk.org/), JavaScript, LAMP, Oracle, XML
3<sup>rd</sup> Party Integrations  | [LinkPoint](http://www.firstdata.com/linkpoint/)

Role of Sudaraka
-|-
Graphics Designer<br> Programmer<br> System Designer

<div class="clear"></div>

Web based, three tire (operator, reseller and consumer) billing platform for a
VoIP communication system. Operator maintain and allocate resources to multiple
resellers who can configure the resources in to various consumer packages and
offer services to the end-user.

Operator and reseller modules shares the business logic model while
functionality is exposed to the end-user module via a
[RESTful](http://en.wikipedia.org/wiki/Representational_state_transfer)
[web service](/assets/gbne-api.html) allowing the reseller to maintain
multiple customer bases on the same platform.

## Features

  * Automated invoice generation for both reseller and end-user.
  * Customizable calling plan/package system.
  * API system for developing reseller/consumer websites.
  * Group based call rate maintenance.
  * Automated report monitoring and alert system.
  * Voice-mail over web (via [Asterisk](http://www.asterisk.org/))

## Reseller/Consumer websites built on API.

  * [iDialIP](/portfolio/idial-ip)
  * [iDialDirect](/portfolio/idial-direct)
  * [iUSA](/portfolio/iusa)
  * [VoizNet](/portfolio/voiznet)
  * VoipPal
  * Rocket Voip
  * Your Call Saver
  * Freedom Talk
  * VP Call
