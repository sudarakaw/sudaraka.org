+++
title = "Dealtwist "
description = "Dealtwist - Deal-a-Day shopping web site."
date = 2009-05-15T06:00:00+05:30

[extra]
keywords = "dealtwist,web,deal-a-day,php,mysql,jquery,html,smarty,css,authorize.net,linkpoint,first data,ups,paypal,google checkout"
css = [ "project" ]
+++


![](/assets/img/portfolio/dealtwist.jpg)

Summary
-|-
Url  | http://www.dealtwist.com/
Released  | 2009
Second Release  | 2011
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Smarty
3<sup>rd</sup> Party Integrations  | Authorize.Net, [First Data](http://www.firstdata.com/), Google Checkout, PayPal, UPS

Role of Sudaraka
-|-
Database Designer<br> Graphics Designer (for first release)<br> Programmer<br> System Designer

<div class="clear"></div>

Deal-a-Day shopping web site.

## Features

  * Product maintenance with calendar based daily deal creation
  * Shopping cart system
  * Custom CMS for static pages and banners
  * Simple blog system for products
  * Customer & order maintenance system
  * Shipping label creation

