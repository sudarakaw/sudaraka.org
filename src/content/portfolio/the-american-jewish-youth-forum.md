+++
title = "The American Jewish Youth Forum"
description = "The American Jewish Youth Forum"
date = 2011-06-16T04:38:56+05:30

[extra]
keywords = "The American Jewish Youth Forum,web,php,mysql,wordpress,html,css"
css = [ "project" ]
+++


![](/assets/img/portfolio/the-american-jewish-youth-forum.jpg)

Summary
-|-
Url  | http://www.ajyf.org/
Released  | 2011
Status  | On-line
Technologies  | CSS, LAMP, Wordpress

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>

Official web site for a youth organization.
