+++
title = "CBY Mens Club Softball League"
description = "CBY Mens Club Softball League - NYC based softball league"
date = 2013-07-04T17:12:00+05:30

[extra]
keywords = ""
css = [ "project" ]
+++


![](/assets/img/portfolio/cbymcsl-small.jpg)

Summary
-|-
Url  | http://www.cbymcsl.com/
Released  | 2013 - May
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Smarty

Role of Sudaraka
-|-
Database Designer<br>Programmer<br>System Designer

<div class="clear"></div>

Official Web site for an softball league in the NYC.

![](/assets/img/portfolio/cbymcsl.jpg)

<div class="clear"></div>
