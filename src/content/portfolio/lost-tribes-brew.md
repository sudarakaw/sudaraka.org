+++
title = "Lost Tribes Brew"
description = "Lost Tribes Brew - promotional web site for a brewery."
date = 2011-02-15T23:09:17+05:30

[extra]
keywords = "Lost Tribes Brew,web,brewery,beer,php,mysql,wordpress,jquery,html,css,google maps api"
css = [ "project" ]
+++


![](/assets/img/portfolio/lost-tribes-brew.jpg)

Summary
-|-
Url  | http://www.losttribesbrew.com/
Released  | 2011
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Wordpress
3<sup>rd</sup> Party Integrations  | Google Maps API

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>

Promotional web site for a brewery.

Web site also include a custom made Wordpress plug-in for search an area using
[Google Maps API](https://developers.google.com/maps/) to find the locations
serving Lost Tribes Brew beverages.
