+++
title = "Komarov Inc."
description = "Komarov Inc. - Web site for a costume designer, showcasing costumes from many seasonal collections they put out through the year."
date = 2008-11-15T23:18:40+05:30

[extra]
keywords = "komarov,web,costumer designer,php,mysql,html,javascript,css,smarty"
css = [ "project" ]
+++


![](/assets/img/portfolio/komarov-inc.jpg)

Summary
-|-
Url  | http://www.komarovinc.com/
Released  | 2008
Status  | Off-line<br> (different version on-line)
Technologies  | CSS, JavaScript, LAMP, Smarty

Role of Sudaraka
-|-
Database Designer<br> Programmer<br> System Designer

![](/assets/img/portfolio/komarov-inc-2.jpg)

<div class="clear"></div>

Web site for a costume designer, showcasing costumes from many seasonal
collections they put out through the year.
