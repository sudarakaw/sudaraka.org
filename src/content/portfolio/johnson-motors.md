+++
title = "Johnson Motors"
description = "Johnson Motors - Complete shopping cart system for a clothing line."
date = 2009-10-15T01:13:14+05:30

[extra]
keywords = "Johnson Motors,web,shopping cart,clothing,php,mysql,jquery,html,css,authorize.net,fedex,ups,usps"
css = [ "project" ]
+++


![](/assets/img/portfolio/johnson-motors.jpg)

Summary
-|-
Url  | http://www.johnsonmotorsinc.com/shop/
Released  | 2009
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Smarty
3<sup>rd</sup> Party Integrations  | Authorize.Net, FeDex, UPS, USPS

Role of Sudaraka
-|-
Database Designer<br> Programmer<br> System Designer

![](/assets/img/portfolio/johnson-motors-2.jpg)

<div class="clear"></div>

Complete shopping cart system for a clothing line.
