+++
title = "MIAMICA"
description = "Miamica - Retail shopping web site for cloths and accessories."
date = 2011-08-15T04:49:44+05:30

[extra]
keywords = "Miamica,web,shopping,retail,php,mysql,wordpress,x-cart,css,html"
css = [ "project" ]
+++


![](/assets/img/portfolio/miamica.jpg)

Summary
-|-
Url  | http://miamica.com/
Released  | 2012 - May
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Wordpress
3<sup>rd</sup> Party Integrations  | Google Maps API, [X-Cart](http://www.x-cart.com/)

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>

Retail shopping web site for cloths and accessories.

Application consists of 2 sections:

  - Main shopping cart and store application is built using the
    [X-Cart](http://www.x-cart.com/) solution.
  - The media section built using Wordpress (and YubePress plug-in). And custom
    made plug-in to locate retail stores in your area using Google Maps API.

