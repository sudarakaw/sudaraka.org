+++
title = "EskimoAutoAir.Com"
description = "EskimoAutoAir.Com - Complete inventory, sales, shipping  and order handling, accounting and CRM system for auto part retailer. (Clone of RadiatorsWarehouse.Com)"
date = 2013-07-04T06:30:00+05:30

[extra]
keywords = "eskimo auto air,web,auto part,retail,pos,shopping cart,sales,shipping,account,inventory,crm,php,mysql,javascript,css,html,printing"
css = [ "project" ]
+++


![](/assets/img/portfolio/eskimoautoair.jpg)

Summary
-|-
Url  | http://eskimoautoair.com/
Released  | 2013 - February
Status  | On-line
Technologies  | CSS, JavaScript, LAMP, XML
3<sup>rd</sup> Party Integrations  | Authorize.Net, DHL, FedEx, [OnTrac](http://www.ontrac.com/), UPS

Role of Sudaraka
-|-
Database Designer<br> Programmer<br> System Designer

<div class="clear"></div>

A clone of [RadiatorsWarehouse.Com](/portfolio/radiatorswarehouse-com) with the
same exact functionality as of the time of launch.
