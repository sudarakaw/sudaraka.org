+++
title = "RadiatorsWarehouse.Com"
description = "RadiatorsWarehouse.Com - Complete inventory, sales, shipping  and order handling, accounting and CRM system for auto part retailer."
date = 2007-05-15T02:40:10+05:30

[extra]
keywords = "radiators warehouse,web,auto part,warehouse,retail,pos,shopping cart,sales,shipping,account,inventory,crm,php,mysql,javascript,css,html,printing"
css = [ "project" ]
+++


![](/assets/img/portfolio/radiatorswarehouse-com.jpg)

Summary
-|-|
Url  | http://www.radiatorswarehouse.com/
Released  | 2007
Major Releases  | 2008 - POS System, 2010 - Inventory and Accounting
Status  | On-line
Technologies  | CSS, JavaScript, LAMP, XML
3<sup>rd</sup> Party Integrations  | Authorize.Net, DHL, FedEx, [OnTrac](http://www.ontrac.com/), UPS

Role of Sudaraka
-|-|
Database Designer<br> Programmer<br> System Designer

<div class="clear"></div>

A complete inventory, sales, shipping  and order handling, accounting and CRM
system with many other small features that keeps adding and upgrading since 2007.

In 2007 the project started as a simple shopping cart for a auto part
(radiator) retailer, and as the business grows the client requested more and
more features added to the system. As of 2012 the application has become the
complete solution for their day-to-day operations.

## Features

  * Inventory organized into categories multi-level hierarchy and types, and
    multiple warehouse support.
  * On-line shopping cart.
  * Point Of Sale system.
  * Printable invoice, Shipping and order label generation.
  * Inventory monitoring and automatic purchase order generation.
  * Supplier maintenance and accounts payable system.
  * Automatic price adjustment using predefined price formulas.
  * Customer maintenance and payment, accounting system.
  * Order maintenance with estimates, order cloning and voiding.
  * Vehicle history and service record keeping.
  * Reports for sales and inventory modules.
  * Ship using FedEx, DHL, UPS or OnTrac.

&nbsp;

![](/assets/img/portfolio/radiatorswarehouse-com-2.jpg)

![](/assets/img/portfolio/radiatorswarehouse-com-3.jpg)

<div class="clear"></div>
