+++
title = "Luxury Exhibit"
description = "Luxury Exhibit - On-line shopping web site for a business of luxury items, diamonds, watches, etc."
date = 2011-03-15T00:27:12+05:30

[extra]
keywords = "Luxury Exhibit,web,shopping,luxury,watches,php,mysql,x-cart,jquery,wordpress,html,css,smarty"
css = [ "project" ]
+++


![](/assets/img/portfolio/luxury-exhibit.jpg)

Summary
-|-
Url  | http://luxuryexhibit.com/
Released  | 2011
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Smarty, Wordpress
3<sup>rd</sup> Party Integrations  | Google Products Feed, James List Feed, [X-Cart](http://www.x-cart.com/)

Role of Sudaraka
-|-
Programmer<br> System Designer (for diamond search tool)<br> Database Designer (for diamond search tool)

<div class="clear"></div>

On-line shopping web site for a business of luxury items, diamonds, watches, etc.

Application consists of 3 sections:

  - Main shopping cart and store application is built using the [X-Cart](http://www.x-cart.com/) solution.
  - Diamonds search tool built using Sudaraka's experimental web framework.
  - A blog for the company built using Wordpress.

&nbsp;

![](/assets/img/portfolio/luxury-exhibit-2.jpg)

<div class="clear"></div>
