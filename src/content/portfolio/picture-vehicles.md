+++
title = "Picture Vehicles"
description = "Searchable/Browsable listing of different types of vehicles since 1930."
date = 2005-06-15T18:01:56+05:30

[extra]
keywords = "web,project,php,mysql,html"
css = [ "project" ]
+++


![](/assets/img/portfolio/picture-vehicles.jpg)

Summary
-|-
Released  | 2005
Status  | Off-line<br> (different version on-line)
Technologies  | LAMP

Role of Sudaraka
-|-
Database Designer<br> Programmer

![](/assets/img/portfolio/picture-vehicles-2.jpg)

<div class="clear"></div>

Listing of images and statistics of various types of vehicles dating back from 1930.

List of vehicles and their images were controlled by a back-end administration
interface.
