+++
title = "International Dial Direct"
description = "Billing system for a telephony company"
date = 2004-05-15T02:35:22+05:30

[extra]
keywords = "web,project,php,mysql,javascript,html"
css = [ "project" ]
+++


![](/assets/img/portfolio/international-dial-direct.jpg)

Summary
-|-
Url  | http://www.idd4u.com/
Released  | 2004
Status  | Off-line
Technologies  | Flash, LAMP
3<sup>rd</sup> Party Integrations  | [LinkPoint](http://www.firstdata.com/linkpoint/), PayPal

Role of Sudaraka
-|-
Database Designer<br> Programmer

<div class="clear"></div>


Billing system for a telephony company, web site enable the customers to sign-up
and purchase prepaid credit to use in international calls.  Customers could also
see their usage, rate available for different destinations and re-charge their
account balance.

For the company, a secured administration module enabled the maintenance of
customers, update call rates, authorize/decline transactions and getting reports
of usage and revenue.

First version of the system was integrated with
[PayPal](https://www.paypal.com/) for payment processing, and it was later
changed to a [LinkPoint](http://www.firstdata.com/linkpoint/) based (YourPay)
system.
