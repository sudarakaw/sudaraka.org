+++
title = "Watch Repair Co."
description = "Watch Repair Co. - web site for a company that repair and maintain watches."
date = 2012-04-15T12:57:09+05:30

[extra]
keywords = "Watch Repair,watch,repair,web,php,mysql,wordpress,jquery,css,html"
css = [ "project" ]
+++


![](/assets/img/portfolio/watch-repair.jpg)

Summary
-|-
Url  | http://watchrepairco.com/
Released  | 2012 - May
Status  | On-line
Technologies  | CSS, jQuery, LAMP, Wordpress

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>

Web site for a company that repair and maintain watches.
