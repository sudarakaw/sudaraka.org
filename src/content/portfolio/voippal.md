+++
title = "VoipPAL"
description = "VoipPAL - Voip service provider web site developed on the GlobalNet VoIP Billing Platform."
date = 2007-05-15T07:41:46+05:30

[extra]
keywords = "voippal,web,voip,end user,billing,php,mysql,html,flash"
css = [ "project" ]
+++


![](/assets/img/portfolio/voippal.jpg)

Summary
-|-
Url  | http://www.voippal.com/
Released  | 2006
Status  | Off-line<br> (different version on-line)
Technologies  | Flash, LAMP

Role of Sudaraka
-|-
Programmer

<div class="clear"></div>

End user VoIP solution build on [GlobalNet VoIP Billing
Platform](/portfolio/globalnet-voip-billing-platform).
