+++
title = "Pasadena Bar Association"
description = "Pasadena Bar Association - web site for search attorney contact details, and listing of news and events of the association."
date = 2008-03-15T19:52:42+05:30

[extra]
keywords = "Pasadena Bar Association,web,attoney,search,news,event,listing,php,mysql,javascript,html,css,smarty"
css = [ "project" ]
+++


![](/assets/img/portfolio/pasadena-bar-association.jpg)

Summary
-|-
Url  | http://www.pasadenabar.org/
Released  | 2008
Status  | Off-line<br> (different version on-line)
Technologies  | CSS, JavaScript, LAMP, Smarty

Role of Sudaraka
-|-
Database Designer<br> Programmer

<div class="clear"></div>

Search attorney contact details, and listing of news and events of the association.
