+++
title = "OpenSprinkler Pi - Monitor"
description = "OpenSprinkler Pi - Monitor - Web based Free Software application to monitor and control OpenSprinkler hardware via Raspberry Pi"
date = 2013-07-04T19:21:00+05:30

[extra]
keywords = "opensprinkler,raspberry pi,monitor,linux,web,http server,python,google api,calendar,gpio"
css = [ "project" ]
+++


![](/assets/img/portfolio/opensprinkler-pi-monitor.jpg)

Summary
-|-
Released  | 2013 - May
License  | GPLv3 or later
Source  | http://git.sudaraka.org/opensprinkler/ospi-monitor/
Technologies  | Python
3<sup>rd</sup> Party Integrations  | Raspberry Pi GPIO, OpenSprinkler (Hardware), Google Calendar

Role of Sudaraka
-|-
Programmer<br> System Designer

<div class="clear"></div>

Based on the sample code from original OpenSprinkler Pi code base, this program
provide more standardized web server and modern UI.

**Objectives:**

  * Make the sample applications more usable/useful
  * Integrate modern UI with mobile support
  * Combine manual and Google calendar controls into one application
  * Release under GPLv3 or later free software license
