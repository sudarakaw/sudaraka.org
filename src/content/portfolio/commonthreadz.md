+++
title = "CommonThreadz "
description = "CommonThreadz - fund raising web site for a nonprofit helping orphans & vulnerable children in developing nations."
date = 2008-10-15T21:02:00+05:30

[extra]
keywords = "CommonThreadz,web,fund raising,project,gift,paypal,php,mysql,javascript,html,css,smarty"
css = [ "project" ]
+++


![](/assets/img/portfolio/commonthreadz.jpg)

Summary
-|-
Url  | http://www.commonthreadz.org/
Released  | 2008
Status  | On-line
Technologies  | CSS, LAMP, Smarty
3<sup>rd</sup> Party Integrations  | PayPal

Role of Sudaraka
-|-
Database Designer<br> Programmer

<div class="clear"></div>

Fund raising web site for a nonprofit helping orphans & vulnerable children in
developing nations.

System allows the supporter to register as individuals or groups and host their
own fund raising campaigns.
