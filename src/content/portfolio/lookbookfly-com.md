+++
title = "LookBookFly.com"
description = "Static web site for air line booking agency"
date = 2005-02-15T03:11:59+05:30

[extra]
keywords = "web,project,php,javascript,html"
css = [ "project" ]
+++


![](/assets/img/portfolio/lookbookfly-com.jpg)

Summary
-|-
Url  | http://www.lookbookfly.com/
Released  | 2005
Status  | Off-line

Role of Sudaraka
-|-
Web Developer

<div class="clear"></div>

Air line booking service.
