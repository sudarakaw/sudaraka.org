+++
title = "Web Application Development Expert!, experienced programmer in JavaScript, PHP, Elm - Sudaraka Wijesinghe"
description = "Sudaraka.Org - Personal web site of Sudaraka Wijesinghe - Freelance web application programmer"

[extra]
css = [ "index" ]
keywords = "sudaraka wijesinghe,freelance,programmer,web site,server application,elm,php,javascript,css,html,sudaraka.org"
+++

# Chronicles of a Nocturnal Hermit

<div class="welcome">

_Welcome to my hermitage!, you are at the most significant bit of the interwebz
tube. While you are here, feel free to roam around._

</div>

<div class="message">

&nbsp;

**[Sudaraka Wijesinghe](/about-sudaraka-wijesinghe)** is a computer
**programmer** with experience in:

  * **Web application** programming with modern **PWA** technology
  * **Web front-end** design and programming with **Elm**, **JavaScript**, **CSS3** and **HTML5**
  * **Server side** programming with **PHP**,  **Node.js** and **Rust**

&nbsp;

</div>

<div class="column column-1">

**_Recent Projects_**

  * [OSPi - Monitor](/portfolio/opensprinkler-pi-monitor)
  * [J & P Data Sync](/portfolio/pos-web-db-sync-johnandpetes)
  * [CBYMCSL](/portfolio/cbymcsl)
  * [Eskimo Auto Air](/portfolio/eskimoautoair)
  * [Watch Repair Co.](/portfolio/watch-repair)
  * [CTS Athletics](/portfolio/cts-athletics)
  * [Luxury Exhibit](/portfolio/luxury-exhibit)
  * [Lost Tribes Brew](/portfolio/lost-tribes-brew)
  * [Simcha Day Camp](/portfolio/simcha-day-camp)
  * [Johnson Motors](/portfolio/johnson-motors)
  * [Dealtwist](/portfolio/dealtwist)
  * [Komarov Inc.](/portfolio/komarov-inc)
  * [Hamwoods Group](/portfolio/hamwoods-group)
  * [CommonThreadz](/portfolio/commonthreadz)

</div>

<div class="column column-2">

**_How-To_**

  * [Create self-signed SSL certificate](/how-to/create-self-signed-ssl-certificate)
  * [Install Syslinux and replace GRUB on a running system](/how-to/install-syslinux-and-replace-grub-on-a-running-system)

</div>

<div class="column column-3">

**_The Chronicle_**

  * [How Sudaraka.Org Works](/chronicles/how-sudaraka-org-works)
  * [Launched my personal web site, Finally!](/chronicles/launched-my-personal-web-site-finally)

</div>
