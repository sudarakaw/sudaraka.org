+++
title = "Using LAMP on Hanthana Linux"
description = "An article I wrote for Hanthana Linux Wiki on how to install and setup a basic LAMP server"
date = 2012-05-13T09:15:00+05:30

[extra]
keywords = "LAMP,linux,hanthana linux,install,guide"
+++


An article I wrote for [Hanthana Linux Wiki](http://hanthana.org/wiki/) on how
to install and setup a basic LAMP server.
<!-- more -->

- [හන්තාන ලිනක්ස් මත "ලෑම්ප්" භාවිතය](http://www.hanthana.org/wiki/Si:Using_LAMP_on_Hanthana_Linux)
