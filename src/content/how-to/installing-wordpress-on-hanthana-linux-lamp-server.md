+++
title = "Installing Wordpress on Hanthana Linux LAMP Server"
description = "A link to the article on how to install Wordpress on the Hanthana Linux LAMP server."
date = 2012-05-13T09:15:00+05:30

[extra]
keywords = "wordpress,LAMP,linux,hanthana linux,install,guide"
+++


A follow up to the above article on how to install
[Wordpress](http://wordpress.org/) on the Hanthana Linux LAMP server.
<!-- more -->

- [හන්තාන ලිනක්ස් සහ "ලෑම්ප්" මත වර්ඩ්ප්‍රෙස් (Wordpress) ස්ථාපනය](http://www.hanthana.org/wiki/Si:Installing_Wordpress_on_Hanthana_Linux_LAMP_Server)
