+++
title = "Arch Linux Installation Guide (2019)"
description = "An Arch Linux installation guide without using provided pacstrap scripts"
date = 2020-11-10T16:06:00+05:30

[extra]
keywords = "archlinux,installation,guide,linux"
+++


This is an alternate [Arch Linux](https://www.archlinux.org/) installation guide
created while setting up my workstation. Some of the instructions here might not
suitable for different hardware configurations than what I have.
<!-- more -->

**My Hardware Configuration**

<pre class="console">
<strong>#</strong> <kbd>lspci</kbd>
00:00.0 Host bridge: Intel Corporation Xeon E3-1200 v5/E3-1500 v5/6th Gen Core Processor Host Bridge/DRAM Registers (rev 08)
00:02.0 VGA compatible controller: Intel Corporation Skylake GT2 [HD Graphics 520] (rev 07)
00:14.0 USB controller: Intel Corporation Sunrise Point-LP USB 3.0 xHCI Controller (rev 21)
00:14.2 Signal processing controller: Intel Corporation Sunrise Point-LP Thermal subsystem (rev 21)
00:15.0 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO I2C Controller #0 (rev 21)
00:16.0 Communication controller: Intel Corporation Sunrise Point-LP CSME HECI #1 (rev 21)
00:17.0 SATA controller: Intel Corporation Sunrise Point-LP SATA Controller [AHCI mode] (rev 21)
00:1c.0 PCI bridge: Intel Corporation Sunrise Point-LP PCI Express Root Port #5 (rev f1)
00:1c.5 PCI bridge: Intel Corporation Sunrise Point-LP PCI Express Root Port #6 (rev f1)
00:1f.0 ISA bridge: Intel Corporation Sunrise Point-LP LPC Controller (rev 21)
00:1f.2 Memory controller: Intel Corporation Sunrise Point-LP PMC (rev 21)
00:1f.3 Audio device: Intel Corporation Sunrise Point-LP HD Audio (rev 21)
00:1f.4 SMBus: Intel Corporation Sunrise Point-LP SMBus (rev 21)
01:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 15)
02:00.0 Network controller: Qualcomm Atheros QCA9377 802.11ac Wireless Network Adapter (rev 30)
<strong>#</strong> <kbd>lsusb</kbd>
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 005: ID 0bda:0129 Realtek Semiconductor Corp. RTS5129 Card Reader Controller
Bus 001 Device 004: ID 04f2:b51f Chicony Electronics Co., Ltd HD WebCam
Bus 001 Device 003: ID 04ca:3015 Lite-On Technology Corp.
Bus 001 Device 002: ID 045e:0084 Microsoft Corp. Basic Optical Mouse
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
</pre>

**Summary**

|||
|-|-:|
|**Installation Media** | archlinux-2020.05.01-x86_64.iso|
|**Bootloader** | `systemd-boot`|
|**Kernel** | Arch Linux default kernel|
|**Init** | `systemd`|
|**Shell** | `bash`|
|**Display/Login Manager** | -- none --|
|**Window Manager** | i3-wm on Xorg|
|**Application Launcher** | `rofi`|
|**Terminal** |`tilda`|

For a generic installations instructions see the official [Arch Linux Installation Guide](https://wiki.archlinux.org/index.php/Installation_Guide).

&nbsp;

## Preparing Partitions

**Assumption:** Storage device that we install the OS is connected as `sda`.

When setting up a new workstation, I usually wipe out the all partitions in the
storage device with the following command.

**WARNING: DO NOT** do this if you have any data on the storage device that you
want to keep. (instead you can manually clear the space new partitions.)

<pre class="console">
<strong>#</strong> <kbd>dd if=/dev/zero of=/dev/sda bs=1M count=1</kbd>
</pre>

Create the following GPT partitions using fdisk.

Device Node | Mount Point | Type | Size
-|-|-|-:
/dev/sda1 | /boot | EFI | 100MB
/dev/sda2 | / | Linux | 15GB
/dev/sda3 | /home | Linux | remainder

Note: if you get a warning as follows while creating partitions;

```
Partition #1 contains a vfat signature.

Do you want to reove the signature? [Y]es/[N]o:
```

Choose to remove the signature by pressing `Y` followed by the `Enter` key.

<pre class="console">
<strong>#</strong> <kbd>fdisk /dev/sda</kbd>

<strong>Welcome to fdisk (util-linux 2.33.1).</strong>
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Device does not contain a recognized partition table.
Created a new DOS disklabel with disk identifier 0x640f873c.

Command (m for help): <kbd>g</kbd>
Created a new GPT disklabel (GUID: 7724150B-F303-394A-B1FA-78F31BA5169A).

Command (m for help): <kbd>n</kbd>
Partition number (1-128, default 1):
First sector (2048-125045390, default 2048):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-125045390, default 125045390): <kbd>+100M</kbd>

Created a new partition 1 of type 'Linux filesystem' and of size 100 MiB.

Command (m for help): <kbd>t</kbd>
Selected partition 1
Partition type (type L to list all types): <kbd>1</kbd>
Changed type of partition 'Linux filesystem' to 'EFI System'.

Command (m for help): <kbd>n</kbd>
Partition number (2-128, default 2):
First sector (1050624-125045390, default 1050624):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (1050624-125045390, default 125045390): <kbd>+15G</kbd>

Created a new partition 2 of type 'Linux filesystem' and of size 15 GiB.

Command (m for help): <kbd>n</kbd>
Partition number (3-128, default 3):
First sector (22022144-125045390, default 22022144):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (22022144-125045390, default 125045390):

Created a new partition 3 of type 'Linux filesystem' and of size 49.1 GiB.

Command (m for help): <kbd>w</kbd>
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.

<strong>#</strong> <kbd>fdisk -l /dev/sda</kbd>
<b>Disk /dev/sda: 59.6 GiB, 64023257088 bytes, 125045424 sectors</b>
Disk model: TS64GSSD320
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 7724150B-F303-394A-B1FA-78F31BA5169A

<b>Device        Start       End   Sectors  Size Type</b>
/dev/sda1      2048   1050623   1048576  512M EFI System
/dev/sda2   1050624  22022143  20971520   15G Linux filesystem
/dev/sda3  22022144 125045390 103023247 49.1G Linux filesystem
</pre>

Format each partition as follows:

<pre class="console">
<strong>#</strong> <kbd>mkfs.vfat -F32 /dev/sda1</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>mkfs.ext4 /dev/sda2</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>mkfs.ext4 /dev/sda3</kbd>
<em>--- output not shown here ---</em>
</pre>

NOTE: I'm not creating a swap partition here, because I don't use swap on my
workstations. However, I will be including the instruction to create [swap
file](https://wiki.archlinux.org/index.php/Swap#Swap_file) in a later stage.

&nbsp;

## Prepare for base system installation

### File System

Mount the partition which eventually be the root of the system (`/dev/sda2`) to
`/mnt`, and create sub-directories.

<pre class="console">
<strong>#</strong> <kbd>mount /dev/sda2 /mnt</kbd>
<strong>#</strong> <kbd>mkdir -m 0755 -pv \</kbd>
<strong>></strong> <kbd>/mnt/{boot,dev,etc/systemd/network,home,run,var/{cache/pacman/pkg,lib/pacman,log}}</kbd>
mkdir: created directory '/mnt/boot'
mkdir: created directory '/mnt/dev'
mkdir: created directory '/mnt/etc'
mkdir: created directory '/mnt/etc/systemd'
mkdir: created directory '/mnt/etc/systemd/network'
mkdir: created directory '/mnt/home'
mkdir: created directory '/mnt/run'
mkdir: created directory '/mnt/var'
mkdir: created directory '/mnt/var/cache'
mkdir: created directory '/mnt/var/cache/pacman'
mkdir: created directory '/mnt/var/cache/pacman/pkg'
mkdir: created directory '/mnt/var/lib'
mkdir: created directory '/mnt/var/lib/pacman'
mkdir: created directory '/mnt/var/log'
<strong>#</strong> <kbd>mkdir -m 0750 -pv /mnt/var/lib/iwd</kbd>
mkdir: created directory '/mnt/var/lib/iwd'
<strong>#</strong> <kbd>mkdir -m 0555 -pv /mnt/{proc,sys}</kbd>
mkdir: created directory '/mnt/proc'
mkdir: created directory '/mnt/sys'
<strong>#</strong> <kbd>mkdir -m 1777 -pv /mnt/tmp</kbd>
mkdir: created directory '/mnt/tmp'
</pre>

Mount remaining partitions.

<pre class="console">
<strong>#</strong> <kbd>mount /dev/sda1 /mnt/boot</kbd>
<strong>#</strong> <kbd>mount /dev/sda3 /mnt/home</kbd>
</pre>

#### Swapfile (optional)

<pre class="console">
<strong>#</strong> <kbd>dd if=/dev/zero of=/mnt/swap bs=512M count=1</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>chmod 600 /mnt/swap</kbd>
<strong>#</strong> <kbd>mkswap /mnt/swap</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>swapon /mnt/swap</kbd>
</pre>

### Connect installation environment to LAN/Internet

**Assumption:** Your system connects to LAN/Internet using the interface `wlp1s0`.

Enable WIFI connection using `iwd`. When using a wired connection this steps
in this section may be skipped partly or completely.

#### Connect to WIFI (optional)

<pre class="console">
<strong>#</strong> <kbd>systemctl start iwd</kbd>
<strong>#</strong> <kbd>systemctl start rfkill-unblock@wifi</kbd>
<strong>#</strong> <kbd>iwctl device wlp1s0 set-property Powered on</kbd>
<strong>#</strong> <kbd>iwctl</kbd>
<strong>[iwd]</strong># <kbd>station wlp1s0 connect ESSID</kbd>
Type the network passphrase for ESSID psk.
<strong>Passphrase:</strong> <kbd><em>secret</em></kbd>
<strong>[iwd]</strong># <kbd>exit</kbd>
<strong>#</strong> <kbd>sed '/^Passphrase=/d' /var/lib/iwd/ESSID.psk > /mnt/var/lib/iwd/ESSID.psk</kbd>
<strong>#</strong> <kbd>chmod 0600 /mnt/var/lib/iwd/ESSID.psk</kbd>
</pre>

Then, create a network configuration file (in the target FS), and sym-link it to
the installation system.

<pre class="console">
<strong>#</strong> <kbd>cat > /mnt/etc/systemd/network/<em>any-network-name</em>.network << EOF</kbd>
<kbd>[Match]</kbd>
<kbd>Name=wlp1s0</kbd>
<kbd></kbd>
<kbd>[Network]</kbd>
<kbd>DHCP=yes</kbd>
<strong></strong><kbd>EOF</kbd>
<strong>#</strong> <kbd>ln -sv /mnt/etc/systemd/network/<em>any-network-name</em>.network /etc/systemd/network</kbd>
'/etc/systemd/network/<em>any-network-name</em>.network' -> '/mnt/etc/systemd/network/<em>any-network-name</em>.network'
</pre>

Start `systemd-networkd` and `systemd-resolved` services.

<pre class="console">
<strong>#</strong> <kbd>systemctl start systemd-networkd</kbd>
<strong>#</strong> <kbd>systemctl start systemd-resolved</kbd>
</pre>

**Note:** You might have to symlink `/run/systemd/resolve/stub-resolv.conf` to
`/etc/resolv.conf`

&nbsp;

## Install base system

**[NOTE TO SELF] Use local pacman cache**

Mount cache server into /var/cache/pacman/pkg **of the installation
environment**.

<pre class="console">
<strong>#</strong> <kbd>mkdir -pv /var/lib/pacman/sync</kbd>
mkdir: created directory '/var/lib/pacman/sync'
<strong>#</strong> <kbd>mount -o nolock SERVER_IP:/path/to/sync /var/lib/pacman/sync</kbd>
<strong>#</strong> <kbd>mount -o nolock SERVER_IP:/path/to/cache /var/cache/pacman/pkg</kbd>
</pre>

**[/NOTE TO SELF]**

### Mount pseudo file systems to the target

<pre class="console">
<strong>#</strong> <kbd>mount -t devtmpfs udev /mnt/dev</kbd>
<strong>#</strong> <kbd>mount -t proc proc /mnt/proc</kbd>
<strong>#</strong> <kbd>mount -t sysfs sys /mnt/sys</kbd>
<strong>#</strong> <kbd>mount -t tmpfs tmp /mnt/tmp</kbd>
</pre>

For the installation you can use mirror closest to you and later do a
full system upgrade using an up-to-date repository. Select the closest
mirror and move it to the top of `/etc/pacman.d/mirrorlist`.

Note: install all the packages from `base-devel` group.

<pre class="console">
<strong>#</strong> <kbd>pacman -Sy -r /mnt linux linux-firmware intel-ucode iwd dhcpcd nfs-utils base-devel</kbd>
<em>--- output not shown here ---</em>
</pre>

&nbsp;

## Make the new installation bootable

### Bootloader configuration

Make UEFI boot directories for `systemd-boot` (without using `bootctl`), copy
boot loader firmware and create loader configuration files manually.

<pre class="console">
<strong>#</strong> <kbd>mkdir -pv /mnt/boot/{EFI/systemd,loader/entries}</kbd>
mkdir: created directory '/mnt/boot/EFI'
mkdir: created directory '/mnt/boot/EFI/systemd'
mkdir: created directory '/mnt/boot/loader'
mkdir: created directory '/mnt/boot/loader/entries'
<strong>#</strong> <kbd>cp /mnt/lib/systemd/boot/efi/systemd-bootx64.efi /mnt/boot/EFI/systemd</kbd>
<strong>#</strong> <kbd>cat > /mnt/boot/loader/loader.conf << EOF</kbd>
<strong>></strong> <kbd>default <em>loader-name</em></kbd>
<strong>></strong> <kbd>timeout 0</kbd>
<strong>></strong> <kbd>EOF</kbd>
<strong>#</strong> <kbd>cat > /mnt/boot/loader/entries/<em>loader-name</em>.conf << EOF</kbd>
<strong>></strong> <kbd>title Arch Linux</kbd>
<strong>></strong> <kbd>linux /vmlinuz-linux</kbd>
<strong>></strong> <kbd>initrd /intel-ucode.img</kbd>
<strong>></strong> <kbd>initrd /initramfs-linux.img</kbd>
<strong>></strong> <kbd>options root=/dev/sda2 init=/usr/lib/systemd/systemd rw quiet</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

If there are any `dump-*` files in the `/sys/firmware/efi/efivars` directory,
delete them all, and then run;

<pre class="console">
<strong>#</strong> <kbd>efibootmgr --create --disk /dev/sda --part 1 --loader /EFI/systemd/systemd-bootx64.efi --label "Arch Linux"</kbd>
</pre>

&nbsp;

## Setup system via chroot

<pre class="console">
<strong>#</strong> <kbd>chroot /mnt /bin/bash</kbd>
</pre>

### Connect the system to network on boot

<pre class="console">
<strong>#</strong> <kbd>systemctl enable iwd systemd-networkd systemd-resolved</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>ln -sfv /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf</kbd>
'/etc/resolv.conf' -> '/run/systemd/resolve/stub-resolv.conf'
</pre>

### Initialize pacman keys

<pre class="console">
<strong>#</strong> <kbd>pacman-key --init</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>pacman-key --populate archlinux</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>sed -i 's/^#\(Server =.\+\)$/\1/' /etc/pacman.d/mirrorlist</kbd>
</pre>

Although it might not be necessary for a new installation, do a system update.

<pre class="console">
<strong>#</strong> <kbd>pacman -Syu</kbd>
<em>--- output not shown here ---</em>
</pre>


### Create `/etc/fstab`

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/fstab << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>/dev/sda2 / ext4 rw,relatime,noatime,nodiratime,discard,errors=remount-ro 0 1</kbd>
<strong>></strong> <kbd>/dev/sda1 /boot vfat rw,relatime,fmask=0022,dmask=0022,shortname=mixed,errors=remount-ro 0 2</kbd>
<strong>></strong> <kbd>/dev/sda3 /home ext4 rw,relatime,noatime,nodiratime,discard,errors=remount-ro 0 2</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

**[NOTE TO SELF] Mount local pacman cache when required**

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/fstab << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>SERVER_IP:/path/to/sync /var/lib/pacman/sync nfs noauto,x-systemd.automount,noexec,nolock,noatime,nodiratime,rsize=32768,wsize=32768,timeo=14,nfsvers=3 0 0</kbd>
<strong>></strong> <kbd>SERVER_IP:/path/to/cache /var/cache/pacman/pkg nfs noauto,x-systemd.automount,noexec,nolock,noatime,nodiratime,rsize=32768,wsize=32768,timeo=14,nfsvers=3 0 0</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

**[/NOTE TO SELF]**

### Set `/etc/hostname`

<pre class="console">
<strong>#</strong> <kbd>echo '<em>machine-name</em>' > /etc/hostname</kbd>
</pre>

### Time zone and clock settings

Setting time zone to Asia/Colombo. Make sure you change the `Asia/Colombo` text
& the zoneinfo directory to match your time zone.

<pre class="console">
<strong>#</strong> <kbd>echo 'Asia/Colombo' > /etc/timezone</kbd>
<strong>#</strong> <kbd>rm /etc/localtime</kbd>
<strong>#</strong> <kbd>ln -sv /usr/share/zoneinfo/Asia/Colombo /etc/localtime</kbd>
'/etc/localtime' -> '/usr/share/zoneinfo/Asia/Colombo'
<strong>#</strong> <kbd>hwclock --hctosys --utc</kbd>
<strong>#</strong> <kbd>date</kbd>
Sun Mar  3 01:59:02 +0530 2019
</pre>

**[NOTE TO SELF] Enable synchronization with local NTP server**

Set `systemd-timesyncd` to synchronize system time via NTP.

<pre class="console">
<strong>#</strong> <kbd>sed -i 's/^#\(NTP=\)/\1<em>NTP_SERTVER_IP</em>/; s/^#\(FallbackNTP=.\+\)/\1/' /etc/systemd/timesyncd.conf</kbd>
<strong>#</strong> <kbd>systemctl enable systemd-timesyncd</kbd>
</pre>

**[/NOTE TO SELF]**

Optionally, if the system time need to be changed:

<pre class="console">
<strong>#</strong> <kbd>date MMDDhhmmYYYY</kbd>
<strong>#</strong> <kbd>hwclock --systohc --utc</kbd>
<strong>#</strong> <kbd>date</kbd>
DDD MMM DD hh:mm:?? +0530 YYYY
</pre>

### Generate locale

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/locale.gen << EOF</kbd>
<strong>></strong> <kbd>en_US.UTF-8 UTF-8</kbd>
<strong>></strong> <kbd>si_LK.UTF-8 UTF-8</kbd>
<strong>></strong> <kbd>EOF</kbd>
<strong>#</strong> <kbd>locale-gen</kbd>
Generating locales...
  en_US.UTF-8 UTF-8... done
  si_LK.UTF-8 UTF-8... done
Generation complete.
</pre>

Note: at this point `systemd` will not allow us to set locale using `localectl`.
We will do that after booting into the new system.


### systemd adjustments

#### Stop console clearing at login prompt

<pre class="console">
<strong>#</strong> <kbd>mkdir -pv /etc/systemd/system/getty@tty1.service.d</kbd>
mkdir: created directory '/etc/systemd/system/getty@tty1.service.d'
<strong>#</strong> <kbd>cat > /etc/systemd/system/getty@tty1.service.d/noclear.conf << EOF</kbd>
<strong>></strong> <kbd>[Service]</kbd>
<strong>></strong> <kbd>TTYVTDisallocate=no</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

#### Limit number of TTYs

<pre class="console">
<strong>#</strong> <kbd>sed -i 's/#\(NAutoVTs=\).*/\12/' /etc/systemd/logind.conf</kbd>
</pre>

#### Limit journal disk usage

<pre class="console">
<strong>#</strong> <kbd>sed -i 's/#\(SystemMaxUse=\).*/\116M/' /etc/systemd/journald.conf</kbd>
</pre>

#### Mask unnecessary systemd units that cause delay in system startup

<pre class="console">
<strong>#</strong> <kbd>systemctl mask lvm2-monitor systemd-backlight@</kbd>
</pre>

### pacman configurations

**[NOTE TO SELF]** Block `pacman` from installing `mono` and/or `java` runtime.

<pre class="console">
<strong>#</strong> <kbd>sed -i 's/#\(IgnorePkg\s*=.*\)/\1 mono mono-* *-openjdk-headless/' /etc/pacman.conf</kbd>
</pre>

**[/NOTE TO SELF]**

Enable color in `pacman` output

<pre class="console">
<strong>#</strong> <kbd>sed -i 's/#\(Color\|VerbosePkgLists\)/\1/' /etc/pacman.conf</kbd>
</pre>

### Disable webcam

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/modprobe.d/modprobe.conf << EOF</kbd>
<strong>></strong> <kbd>blacklist uvcvideo</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

### Create user(s)

<pre class="console">
<strong>#</strong> <kbd>useradd -m -s /bin/bash -G systemd-journal,wheel -U <em>new_username</em></kbd>
<strong>#</strong> <kbd>passwd new_username</kbd>
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
</pre>

#### `sudo` capability

Add this user to `/etc/sudoers` as we will be blocking root from login in the
system.

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/sudoers << EOF</kbd>
<strong>></strong> <kbd><em>new_username</em> <em>machine_name</em>=(root) /usr/bin/pacman</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

You may allow regular user to gain access to a root shell via `sudo` during the
setup in order to prevent any lock-up situation;

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/sudoers << EOF</kbd>
<strong>></strong> <kbd><em>new_username</em> <em>machine_name</em>=(root) /usr/bin/su -ls /usr/bin/bash</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

**WARNING:** however above may leave your system insecure in some situations and
**you may remove this capability** once it is no longer needed.

Lockout `root` from login into console or from SSH in. Optionally remove
the password hash for root from `/etc/shadow`.

<pre class="console">
<strong>#</strong> <kbd>rm /etc/securetty</kbd>
<strong>#</strong> <kbd>sed -i 's#\(root:x:0:0::/root:/bin/\).\+#\1false#' /etc/passwd</kbd>
<strong>#</strong> <kbd>sed -i 's/\(root:\)[^:]*\(:.\+\)/\1!!\2/' /etc/shadow</kbd>
</pre>

#### Disable shell history at system level

Note: users may enable/override this from their shell configuration.

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/profile << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd># Disable shell history recording for all users</kbd>
<strong>></strong> <kbd>unset HISTFILE</kbd>
<strong>></strong> <kbd>export HISTFILESIZE=0</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

At this point you should reboot into newly installed system, and login as a
regular user.

### Exit chroot, unmount and reboot

<pre class="console">
<strong>#</strong> <kbd>exit</kbd>
exit
<strong>#</strong> <kbd>umount -Rv /mnt</kbd>
unmount: /mnt/boot unmounted
unmount: /mnt/home unmounted
unmount: /mnt/dev unmounted
unmount: /mnt/proc unmounted
unmount: /mnt/sys unmounted
unmount: /mnt/tmp unmounted
unmount: /mnt unmounted
<strong>#</strong> <kbd>systemctl reboot</kbd>
</pre>

## First-boot configuration

### Unblock RF-Kill

<pre class="console">
<strong>#</strong> <kbd>systemctl start rfkill-unblock@wifi</kbd>
<strong>#</strong> <kbd>systemctl restart iwd</kbd>
<strong>#</strong> <kbd>systemctl restart systemd-networkd</kbd>
<strong>#</strong> <kbd>systemctl restart systemd-resolved</kbd>
</pre>

### Set locale

<pre class="console">
<strong>#</strong> <kbd>localectl set-locale LANG=en_US.UTF-8</kbd>
</pre>

### pacman mirror list

Setup `reflector` to update pacman mirror list every time system boots.

<pre class="console">
<strong>#</strong> <kbd>pacman -S reflector</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>cat > /etc/systemd/system/reflector.service << EOF</kbd>
<strong>></strong> <kbd>[Unit]</kbd>
<strong>></strong> <kbd>Description=Pacman mirrorlist update</kbd>
<strong>></strong> <kbd>Wants=network-online.target</kbd>
<strong>></strong> <kbd>After=network-online.target</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>[Service]</kbd>
<strong>></strong> <kbd>Type=oneshot</kbd>
<strong>></strong> <kbd>ExecStart=/usr/bin/reflector --protocol https --latest 30 --number 20 --fastest 10 --sort rate --save /etc/pacman.d/mirrorlist</kbd>
<strong>></strong> <kbd>EOF</kbd>
<strong>#</strong> <kbd>cat > /etc/systemd/system/reflector.timer << EOF</kbd>
<strong>></strong> <kbd>[Unit]</kbd>
<strong>></strong> <kbd>Description=Pacman mirrorlist update runner</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>[Timer]</kbd>
<strong>></strong> <kbd>OnCalendar=daily</kbd>
<strong>></strong> <kbd>RandomizedDelaySec=1day</kbd>
<strong>></strong> <kbd>Persistent=true</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>[Install]</kbd>
<strong>></strong> <kbd>RequiredBy=timers.target</kbd>
<strong>></strong> <kbd>EOF</kbd>
<strong>#</strong> <kbd>systemctl enable reflector.timer</kbd>
</pre>

### Install `yay` AUR helper

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S git</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>git clone https://aur.archlinux.org/yay /tmp/yay</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>cd /tmp/yay</kbd>
<strong>$</strong> <kbd>makepkg -si</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>sudo pacman -Rcnsu go</kbd>
<em>--- output not shown here ---</em>
</pre>

Use `yay` to reinstall `yay`, so that all *AUR* packages in out system is
properly installed using `yay`.

<pre class="console">
<strong>$</strong> <kbd>yay --removemake -S yay</kbd>
<em>--- output not shown here ---</em>
</pre>

Optionally, do a full system update using `yay`.

<pre class="console">
<strong>$</strong> <kbd>yay --removemake --combinedupgrade -Syu</kbd>
<em>--- output not shown here ---</em>
</pre>

&nbsp;

## GUI

### Xorg

Install appropriate video driver.

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S xf86-video-<em>vendor</em></kbd>
<em>--- output not shown here ---</em>
</pre>

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S xorg-{server,xinit}</kbd>
<em>--- output not shown here ---</em>
</pre>

Configure Xorg to start on user login.

<pre class="console">
<strong>$</strong> <kbd>cat >> ~/.bash_profile << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
<strong>></strong> <kbd>  exec startx</kbd>
<strong>></strong> <kbd>fi</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

#### Fonts

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S ttf-{dejavu,droid}</kbd>
<em>--- output not shown here ---</em>
</pre>

### i3 Window Manager

Install `i3` window manager, and set `Alt` key as `$mod`.

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S i3-wm</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>cat > ~/.xinitrc << EOF</kbd>
<strong>></strong> <kbd>exec i3 > /dev/null 2>&1</kbd>
<strong>></strong> <kbd>EOF</kbd>
<strong>$</strong> <kbd>mkdir -pv ~/.config/i3</kbd>
mkdir: created directory '/home/suda/.config/i3'
<strong>$</strong> <kbd>cat > ~/.config/i3/config << EOF</kbd>
<strong>></strong> <kbd>set $mod Mod1</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

#### `rofi` launcher

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S rofi</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>cat >> ~/.config/i3/config << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>bindsym $mod+d exec --no-startup-id rofi -show run</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

#### Terminal

Install `tilda` terminal emulator & launch it when `i3` starts.

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S tilda</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>cat >> ~/.config/i3/config << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>exec --no-startup-id tilda</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

&nbsp;

## Sound: ALSA Support

ALSA support is enabled in the kernel by default. Following installs a set of
utilities for configuration and integration need that applies to all hardware.

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S alsa-{utils,oss,plugins} pulseaudio{,-alsa}</kbd>
<em>--- output not shown here ---</em>
</pre>

Do the necessary adjustments using **alsamixer** and test and save settings.

<pre class="console">
<strong>$</strong> <kbd>alsamixer</kbd>
<strong>$</strong> <kbd>speaker-test -c 2</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>alsactl store</kbd>
</pre>

&nbsp;

---

**Following instructions are for my personal setup choices.**

#### Bash completion

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S bash-completion</kbd>
<em>--- output not shown here ---</em>
</pre>

#### Man pages & viewer

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S man-db</kbd>
<em>--- output not shown here ---</em>
</pre>

#### `playerctl`

Install `playerctl` to control volume via keyboard.

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S playerctl</kbd>
<em>--- output not shown here ---</em>
</pre>

#### iBus

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S ibus</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>cat >> ~/.bashrc << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>export GTK_IM_MODULE=ibus</kbd>
<strong>></strong> <kbd>export XMODIFIERS=@im=ibus</kbd>
<strong>></strong> <kbd>export QT_IM_MODULE=ibus</kbd>
<strong>></strong> <kbd>export XIM_PROGRAM=/usr/bin/ibus-daemon</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

#### Xorg extra

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S xorg-{xbacklight,xrandr,xset}</kbd>
<em>--- output not shown here ---</em>
</pre>

#### Extra Fonts

<pre class="console">
<strong>$</strong> <kbd>yay -S ttf-lklug otf-fira-{code,mono,sans} otf-font-awesome noto-fonts{,-emoji}</kbd>
<em>--- output not shown here ---</em>
</pre>

#### Programming languages and Development Tools

##### Node.js & pnpm

First install `pnpm` into temporary directory, and then use that installation
as a bootstrap to install `pnpm` under `$HOME`. See [pnpm documentation](https://pnpm.js.org/en/installation) for details.

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S nodejs</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>curl -L https://raw.githubusercontent.com/pnpm/self-installer/master/install.js | \</kbd>
<strong>></strong> <kbd>PNPM_DEST=/tmp PNPM_BIN_DEST=/tmp/bin node</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>/tmp/bin/pnpm -g install pnpm</kbd>
</pre>


##### Rust

Install `rust` via [`rustup`](https://rustup.rs/).

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S rustup</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>rustup default stable</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>rustup update</kbd>
<em>--- output not shown here ---</em>
</pre>


##### PHP & Composer

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S composer</kbd>
<em>--- output not shown here ---</em>
</pre>


#### GUI Applications & Tools

<pre class="console">
<strong>$</strong> <kbd>yay -S claws-mail evince filezilla ghex gimp inkscape gnome-calculator gnucash \</kbd>
<strong>></strong> <kbd>gvfs keepassxc libreoffice-fresh meld thunar transmission-gtk vlc rtv file-roller \</kbd>
<strong>></strong> <kbd>unrar screen mycli dunst</kbd>
<em>--- output not shown here ---</em>
</pre>

##### Firefox nightly

<pre class="console">
<strong>$</strong> <kbd>mkdir -pv $HOME/opt</kbd>
mkdir: created directory '/home/<em>username</em>/opt'
<strong>$</strong> <kbd>curl -sL "https://download.mozilla.org/?product=firefox-nightly-latest-ssl&os=linux64&lang=en-US" | \</kbd>
<strong>></strong> <kbd>tar xj -C $HOME/opt</kbd>
<strong>$</strong> <kbd>sudo pacman -S dbus-glib</kbd>
<em>--- output not shown here ---</em>
</pre>

##### Dropbox

<pre class="console">
<strong>$</strong> <kbd>yay -S dropbox</kbd>
<em>--- output not shown here ---</em>
</pre>

After first run of `dropbox` completes, and files are synchronized exit Dropbox
and, do the following to avoid conflict with Dropbox's auto update.

<pre class="console">
<strong>$</strong> <kbd>rm -fr ~/.dropbox-dist</kbd>
<strong>$</strong> <kbd>install -dm0 ~/.dropbox-dist</kbd>
</pre>

##### Syncthing

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S syncthing</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>mkdir -pv $HOME/Sync</kbd>
mkdir: created directory '/home/<em>username</em>/Sync'
</pre>

#### GTK & Icon themes

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S arc-gtk-theme papirus-icon-theme</kbd>
<em>--- output not shown here ---</em>
</pre>

#### Vim

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S gvim</kbd>
<em>--- output not shown here ---</em>
</pre>

And, also install spell checking tools.

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S aspell-en hunspell-en_US ispell</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>mkdir -pv $HOME/.vim/spell</kbd>
mkdir: created directory '/home/<em>username</em>/.vim'
mkdir: created directory '/home/<em>username</em>/.vim/spell'
<strong>$</strong> <kbd>ln -sv $HOME/.aspell.en.pws $HOME/.vim/spell/en.utf-8.add</kbd>
'/home/<em>username</em>/.vim/spell/en.utf-8.add' -> '/home/<em>username</em>/.aspell.en.pws'
</pre>

#### `conky`

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S conky</kbd>
</pre>

#### Other Tools

<pre class="console">
<strong>$</strong> <kbd>sudo pacman -S iproute2 iputils openssh tree veracrypt</kbd>
</pre>

`Telegram`

<pre class="console">
<strong>$</strong> <kbd>curl -sL https://telegram.org/dl/desktop/linux | tar xJ -C $HOME/opt Telegram/Telegram</kbd>
</pre>
