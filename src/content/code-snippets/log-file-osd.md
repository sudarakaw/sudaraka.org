+++
title = "Log file OSD"
description = "a bash script to tail a log file and send each line to libnotify."
date = 2012-08-12T03:13:00+05:30

[extra]
keywords = "bash,log,tail,libnotify"
+++


When working on something in GUI mode it's not easy to keep track of the
logs. I wrote the following bash script to tail a log file and send each line
to libnotify so I get OSD log events.
<!-- more -->

```bash
#!/bin/bash
# watch-log.sh: monitor text file for a new line of entry and display it via libnotify
#
# Copyright 2011 Sudaraka Wijesinghe
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http: //www.gnu.org/licenses/>.
#
#
# USAGE:
#	./watch-log.sh file [&]
#

if [[ ! -f $1 ]];
then
	echo "Unable to read file $1"
	exit 1
fi

tail -n0 -f $1|while read line;
do
	notify-send "$1" "$line"
done
```
