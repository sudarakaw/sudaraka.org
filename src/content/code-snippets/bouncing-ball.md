+++
title = "Bouncing Ball?"
description = "Pascal program that make a ball bounce in the screen."
date = 2012-08-12T03:13:00+05:30

[extra]
keywords = "pascal,bounce,ball,vga,graphic"
+++


I don't even remember what this does, I couldn't find a compiler or a capable
system try this on. I think it suppose to bounce a ball around the VGA
graphics screen.
<!-- more -->

```pas
uses Graph,crt,vga;

const gfact=1;
      ballfact=10;
      modefact=1;
      delayfact=1;

var
 isize,x,y,xfact,yfact,vgamode:Integer;
 p:pointer;

begin
 setcolor(14);
 circle(ballfact,ballfact,ballfact);
 setfillstyle(1,10);
 floodfill(ballfact,ballfact,14);
 isize:=imagesize(0,0,ballfact*2,ballfact*2);
 getmem(p,isize);
 getimage(0,0,ballfact*2,ballfact*2,p^);

 cleardevice;
 x:=1;
 y:=1;
 xfact:=1;
 yfact:=1;
 setcolor(15);

 Rectangle(0, 0, GetMaxX, GetMaxY);
 line(getmaxx div 2,0,getmaxx div 2,getmaxy);
 line(0,getmaxy div 2,getmaxx,getmaxy div 2);
 circle(getmaxx div 2,getmaxy div 2,50);

 repeat
   begin
     putimage(x,y,p^,modefact);
     delay(delayfact);
     putimage(x,y,p^,modefact);

     x:=x+xfact;
     y:=y+yfact;

     if x+(ballfact)*2>=getmaxx then
       begin
         xfact:=-gfact;
         sound(250);
         delay(25);
         nosound;
       end;

     if x< =0 then
       begin
         xfact:=gfact;
         sound(250);
         delay(25);
         nosound;
       end;

     if y+(ballfact)*2>=getmaxy then
       begin
         yfact:=-gfact;
         sound(250);
         delay(25);
         nosound;
       end;

     if y< =0 then
       begin
         yfact:=gfact;
         sound(250);
         delay(25);
         nosound;
       end;

     if(x=getmaxx div 2)or(y=getmaxy div 2)then
       begin
       end;

   end;
 until keypressed;

 CloseGraph;

end.
```
