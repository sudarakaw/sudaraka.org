+++
title = "BIOS reset"
description = "Assembler code to reset BIOS configuration, written into a Pascal program."
date = 2012-08-12T03:13:00+05:30

[extra]
keywords = "pascal,assembler,reset,bios"
+++


Long time ago there was a time I was fascinated by getting access to password
protected BIOS configurations (CMOS confguration as it was called back then).
I read lot of books about resetting the BIOS/CMOS and found this method to
restore the BIOS configuration to it's factory default.
<!-- more -->

I was using a Dell Pentium PC back then, and code worked on that and few
other 486 machines as I remember.

```pas
{BIOS/CMOS Reset,  Version 1.00}
{[03/18/1998]}
uses
  crt;

var
  ch:char;

begin
  writeln('This program will RESET ALL BIOS/CMOS SETTINGS.');
  write('Are you sure you want to continue? (Y/N):');
  repeat
    begin
      ch:=upcase(readkey);
    end;
  until(ch='Y')or(ch='N');

  writeln;
  writeln;
  if(ch='Y')then
    begin
      asm
        push ax
        mov al,11h
        out 70h,al
        xor al,al
        out 71h,al
        pop ax
      end;

      writeln('BIOS/CMOS settings changed.');
      writeln('Systems will restart now.');
      write('Press a key to continue...');
      ch:=upcase(readkey);

      asm
        push ax
        mov al,0feh
        out 64h,al
        pop ax
      end;

    end;

end.
```
