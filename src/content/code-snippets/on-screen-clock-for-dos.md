+++
title = "On-screen clock for DOS"
description = ""
date = 2012-08-12T03:13:00+05:30

[extra]
keywords = ""
+++


A clock that runs in the background in a DOS system and display the system
time while it's operational.
<!-- more -->

As I remember I used either Microsoft Assembler or Turbo Assembler to compile
this code. Program make use of a technology called "[Terminate and Stay Resident](https://en.wikipedia.org/wiki/Terminate_and_stay_resident_program)"
(TSR) which was used by many applications those days to emulate multi-tasking
in DOS.

```asm
;Clock,  Version 2.00
;Created 05/20/1999
include mac_main.lib
assume	cs:code,ds:code,es:code,ss:code

code	segment
        org	100h

start	proc

do:	initialize
	mov	si,81h
	mov	al,[si]
	cmp	al,'/'
	jnz	nopa
	inc	si
	mov	al,[si]
	cmp	al,'?'
	jz	hl
	cmp	al,'U'
	jz	ul
	cmp	al,'u'
	jz	ul
	print	ivmsg
	jmp	finish

hl:	print	hlmsg
	jmp	finish

ul:	call	lodchk
	cmp	ax,0h
	jnz	nolod
	call	unload
	print	ulmsg
	jmp	finish

nopa:	call	lodchk
	cmp	ax,0h
	jz	loded
	xor	ax,ax
	mov	ds,ax
	mov	si,70h
	mov	di,offset	oldisr
	mov	ax,[si]
	mov	cs:[di],ax
	mov	ax,[si+2]
	mov	cs:[di+2],ax
	mov	si,413h
	mov	ax,[si]
	sub	ax,1h
	mov	[si],ax
	mov	cl,6h
	shl	ax,cl
	mov	es,ax
	push	cs
	pop	ds
	mov	si,offset	clockproc
	xor	di,di
	mov	cx,((offset	idvmsg)-(offset	clockproc))/2
	cld
	rep	movsw
	xor	ax,ax
	mov	ds,ax
	mov	si,70h
	mov	[si],ax
	mov	[si+2],es
	jmp	finish

nolod:	print	nlmsg
	jmp	finish

loded:	print	almsg

finish:	close
start   endp

unload	proc
	push	ax
	push	ds
	push	si
	push	cx
	push	es
	push	di
	xor	ax,ax
	mov	ds,ax
	mov	si,413h
	mov	ax,[si]
	push	ax
	mov	cl,6h
	shl	ax,cl
	mov	es,ax
	pop	ax
	inc	ax
	mov	[si],ax
	mov	si,70h
	mov	di,(offset	oldisr)-(offset	clockproc)
	mov	ax,es:[di]
	mov	[si],ax
	mov	ax,es:[di+2]
	mov	[si+2],ax
	xor	ax,ax
	xor	di,di
	mov	cx,((offset	idvmsg)-(offset	clockproc))/2
	cld
	rep	stosw
	push	cs
	pop	ds
        mov     ax,0b800h
        mov     es,ax
        mov     si,offset   msg2
        mov     di,8ch
        mov     cx,0ah
	cld
	rep	movsw
	pop	di
	pop	es
	pop	cx
	pop	si
	pop	ds
	pop	ax
	ret
unload	endp

lodchk	proc
	push	cx
	push	di
	push	si
	push	ds
	push	es
	xor	ax,ax
	mov	ds,ax
	mov	si,413h
	mov	ax,[si]
	mov	cl,6h
	shl	ax,cl
	mov	es,ax
	push	cs
	pop	ds
	xor	di,di
	mov	si,offset	clockproc
	mov	cx,(offset	msg)-(offset	clockproc)
	cld
	rep	cmpsb
	mov	ax,cx
	pop	es
	pop	ds
	pop	si
	pop	di
	pop	cx
	ret
lodchk	endp

clockproc   proc    far
	push    ax
        push    es
        push    si
        push    di
        push    cx
	push	cs
	pop	ds
        mov     ax,0b800h
        mov     es,ax
        mov     si,(offset   msg)-(offset	clockproc)
        mov     di,8ch
        mov     cx,0ah
        mov    al,0h
        out    70h,al
        in     al,71h
        byte2asc       al,tstr
        mov    ah,tstr[0]
        mov    [si+14],ah
        mov    ah,tstr[1]
        mov    [si+16],ah
        mov    al,2h
        out    70h,al
        in     al,71h
        byte2asc       al,tstr
        mov    ah,tstr[0]
        mov    [si+8],ah
        mov    ah,tstr[1]
        mov    [si+10],ah
        mov    al,4h
        out    70h,al
        in     al,71h
        byte2asc       al,tstr
        mov    ah,tstr[0]
        mov    [si+2],ah
        mov    ah,tstr[1]
        mov    [si+4],ah
        cld
        rep     movsw
        pop     cx
        pop     di
        pop     si
        pop     es
        pop     ax
        iret
clockproc   endp

attr	equ	3eh
msg	db  ' ',attr,'X',attr,'X',attr,':',attr,'X',attr,'X',attr
	db  ':',attr,'X',attr,'X',attr,' ',attr
tstr	db  'XX'
oldisr	dw	?
	dw	?
crnote	db	'Clock,  Version 2.00',0ah,0dh
        db  	'Copyright (c) Sudaraka Wijesinghe 1999.',0ah,0dh,24h
idvmsg  db  	'This program requiers DOS version 5.00 or later.',0ah,0dh,24h
ulmsg	db	0ah,0dh,'Clock Unloaded.',0ah,0dh,24h
hlmsg	db	0ah,0dh,'USAGE: SC2[/?][/U]',0ah,0dh,0ah,0dh
	db	'Without any parameter loads the clock,',0ah,0dh
	db	'	/? - Display this help screen.',0ah,0dh
	db	'	/U - Unload the clock.',0ah,0dh,0ah,0dh
	db	'	   Clock is a program that display the system time in DOS mode.',0ah,0dh
	db	'After the clock is loaded successfully you''ll see a digital clock on the top',0ah,0dh
	db	'right hand corner of your computers screen.',0ah,0dh,24h
ivmsg	db	0ah,0dh,'Invalid parameter.',0ah,0dh,24h
almsg	db	0ah,0dh,'Clock is already loaded.',0ah,0dh,24h
nlmsg	db	0ah,0dh,'Clock is not loaded.',0ah,0dh,24h
msg2	db	' ',1fh,' ',1fh,' ',1fh,'D',1fh,'i',1fh,'n',1fh,'o',1fh,' ',1fh
	db	' ',1fh,' ',1fh

code	ends
        end	start
```
