+++
title = "ESPN cricket score OSD"
description = "Python script that periodically display cricket match score summary from ESPN Cricinfo via libnotify"
date = 2012-11-30T20:34:17+05:30

[extra]
keywords = "python,script,cricket,score,osd,libnotify"
+++


I like to keep up with the cricket match scores when Sri Lanka is playing,
but don't want to sit in-front of a TV or don't have the time for it. I use
[ESPN Cricinfo](http://www.espncricinfo.com/) to check the score, but it is
tedious to go to the site and click around to locate the match each time I
need to know the score.
<!-- more -->

I wrote this python script to pull the score from match page and display on
screen via libnotify.

**Warning:** I strongly discourage using this method for anything other than
personal use, as it is prohibited by the [ESPN Cricinfo terms of use](http://www.espncricinfo.com/ci/content/site/company/terms_use.html)
and on may other web sites on the Internet.

```py
#!/usr/bin/python -tt
#
#  cricinfo-score.py: Fetch match scores from espncricinfo.com and display
#  Copyright (C) 2012 Sudaraka Wijesinghe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#  USAGE: ./cricinfo-score.py <URL>
#

from gtk import gdk
from os.path import *
import sys
import re
import urllib
import time
import pynotify


# Output subroutine
def out(text, title = None, delay = 10000):
  if None == title or 0 == len(title):
    title = 'cricinfo-score.py'

  #print title
  #print text

  notify = pynotify.Notification(title, text)
  # 10s time out
  notify.set_timeout(delay)

  icon = gdk.pixbuf_new_from_file_at_size(abspath(dirname(__file__)) + '/cricket_ball.png', 50, 50)
  notify.set_icon_from_pixbuf(icon)

  try:
    notify.show()
  except:
    sys.exit(3)


# Start main program
if '__main__' == __name__:

  # Initialize notification
  pynotify.init('cricinfo-score.py')

  # Get URL from command line
  if 2 > len(sys.argv):
    out('''
USAGE: cricinfo-score.py <URL>
''')
    sys.exit(1)

  url = sys.argv[1].split('?')[0]
  if not re.match('^http:\/\/www\.espncricinfo\.com\/', url):
    out('Please enter a valid score card URL')
    sys.exit(2)

  url = url + '?template=iframe_desktop'

  try:
    while(1):
      # Fetch score card HTML
      try:
        html = urllib.urlopen(url).read()
      except:
        out('Failed to get HTML from score card page')
        time.sleep(60)
        continue

      # Process HTML
      html = html.split('<!--score top starts-->')[1]
      html = html.split('<!--score top end-->')[0]

      title = re.findall('<p\s+class="desktopHeading">.+:\s+(.+)</p>', html)
      if None != title and 0 < len(title):
        title = title[0].split(',')
        if 0 < len(title):
          title = title[0]

      msg = ''

      team = re.findall('<td\s+class="desktopTextSeparator">(.+)</td>', html)
      if 0 < len(team):
        msg = msg + '<b>' + team[0] + ':</b> '
        if 1 < len(team):
          msg = msg + team[1]

      team = re.findall('<td\s+class="desktopText">(.+)</td>', html)
      if 0 < len(team):
        msg = msg + '\n<b>' + team[0] + ':</b> '
        if 1 < len(team):
          msg = msg + team[1]

      status = re.findall('<td\s+class="desktopStatus".*>(.+)</td>', html)
      if 0 < len(status):
        msg = msg + '\n\n<i>' + status[0] + '</i> '

        # If status reads "Stumps", end of the day
        if 'Stumps' == status[0]:
          out(msg, title, 5 * 60 * 1000)
          break

      result = re.findall('<p\s+class="lveStText">(.+)</p>', html)
      if None != result or 0 < len(result):
        msg = msg + '\n\n<b>' + result[0] + '</b>'

        # If result reads "won by", match is over
        if re.search('won\s+by', result[0]):
          out(msg, title, 5 * 60 * 1000)
          break

      out(msg, title)

      time.sleep(60*5)

  except KeyboardInterrupt, SystemExit:
    sys.exit(0)

  except:
    out('cricinfo-score.py terminated\n' + str(sys.exc_info()))
```
