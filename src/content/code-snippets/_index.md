+++
title = "Code Snippets"
description = "Code snippets from my researches and various scripts that I use for my work."

sort_by = "date"

[extra]
keywords = "sudaraka,code,sample,snippet,scripts,research,pascal,assembler,bash,python"
+++


Code snippets from my researches and various scripts that I use for my work.
Codes samples and programs in this section are available for public use under
GNU GPL v3 or later as described in [here](/copyright#code-samples).
