+++
title = "Arch Linux Installation Guide (using shell scripts, incomplete)"
description = "An Arch Linux installation guide using shell scripts, customized for my hardware and needs"
date = 2014-06-28T03:45:46+05:30

[extra]
keywords = "customized,shell,script,archlinux,installation,guide,linux"
+++


This Arch Linux installation guide is customized for my hardware and my needs.
Anyone may use these instructions to setup a system of their own, but they
are not guaranteed to work on all hardware nor they are the "official" way of
doing things.
<!-- more -->

**Disclaimer:** software components, specially the ones that I block out are
my personal choice, and anyone else referencing this article should
substitute their own choices.

For a generic installations instructions see the official [Arch Linux Installation Guide](https://wiki.archlinux.org/index.php/Installation_Guide).

Installation done using **archlinux-2014.02.01-dual.iso**


## Preparing Partitions

Boot into the live environment, connect to internet and LAN.

<pre class="console">
<strong>#</strong> <kbd>wpa_passphrase &lt;ESSID&gt; &lt;uber_secret_key&gt;|sed '/#psk=/d' > wifi.conf</kbd>
<strong>#</strong> <kbd>rfkill unblock all</kbd>
<strong>#</strong> <kbd>wpa_supplicant -B -D wext -i &lt;wireles_interface&gt; -c wifi.conf</kbd>
<strong>#</strong> <kbd>dhcpcd &lt;wireles_interface&gt;</kbd>
</pre>

_Note: rfkill is optional depending on the hardware._

Then mount the pacman cache from server.

<pre class="console">
<strong>#</strong> <kbd>mkdir -p /var/lib/pacman/sync</kbd>
<strong>#</strong> <kbd>mount -o nolock server_name:/path/to/sync /var/lib/pacman/sync</kbd>
<strong>#</strong> <kbd>mount -o nolock server_name:/path/to/cache /var/cache/pacman/pkg</kbd>
</pre>

Install _git_ and clone the installation scripts from any of the following
repositories.

  * [Bitbucket](https://bitbucket.org/sudaraka/archlinux-install)
  * [Sudaraka.org](http://git.sudaraka.org/scripts/archlinux-install/)

<pre class="console">
<strong>#</strong> <kbd>pacman -S git</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>cd /tmp</kbd>
<strong>#</strong> <kbd>git clone repo_url</kbd>
</pre>

**Important:** scripts should be in the /tmp so they are shared between live and
chroot environments.

## Run Install Scripts

<pre class="console">
<strong>#</strong> <kbd>cd archlinux-install</kbd>
<strong>#</strong> <kbd>./010-prepare-disks.sh sda [sdb]</kbd>
<em>--- output not shown here ---</em>
</pre>

When installing packages, the parameter _kernel_ will make the installation to
include the Archlinux stock kernel. Omit the parameter when using a custom
kernel.

<pre class="console">
<strong>#</strong> <kbd>./020-install-packages.sh [kernel]</kbd>
<em>--- output not shown here ---</em>
</pre>

## Chroot into the new system

010 script should have mounted all the necessary partitions for the chroot
environment, and installations scripts should also be in /tmp

<pre class="console">
<strong>#</strong> <kbd>chroot /mnt /bin/bash</kbd>
<strong>#</strong> <kbd>cd /tmp/archlinux-install</kbd>
</pre>

### Run Configuration Scripts

<pre class="console">
<strong>#</strong> <kbd>./031-configure.sh &lt;machine-name&gt; &lt;username&gt; [rfkill]</kbd>
<em>--- output not shown here ---</em>
</pre>

### Kernel

Compile and install the kernel.

### Create WPA Configuration

If ./wifi.conf already exists in the live environment, it would have been
copied by the installation scripts to the correct location. Otherwise create it
now.

<pre class="console">
<strong>#</strong> <kbd>wpa_passphrase ESSID uber_secret_key|sed '/#psk=/d' > /etc/wpa_supplicant/wifi.conf</kbd>
</pre>

### Exit chroot, unmount and reboot

<pre class="console">
<strong>#</strong> <kbd>exit</kbd>
exit
<strong>#</strong> <kbd>./040-unmount-disks.sh</kbd>
<strong>#</strong> <kbd>reboot</kbd>
</pre>

## Driver installation and configuration

### Sound: ALSA Support

Note: ALSA support must be enabled in the Kernel.

Do the necessary adjustments using **alsamixer** and test and save settings.

<pre class="console">
<strong>$</strong> <kbd>alsamixer</kbd>
<strong>$</strong> <kbd>speaker-test -c 2</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>alsactl store</kbd>
</pre>

Optionally, make the necessary changes to the configuration files to load
ALSA daemon on boot.

### Touch Pad: FSPPS/2 Seltelic

**SINX-450**

<pre class="console">
<strong>#</strong> <kbd>pacman -S xf86-input-synaptics</kbd>
<em>--- output not shown here ---</em>
</pre>

### Video: ATI Graphics Card

**IBM R52**

<pre class="console">
<strong>#</strong> <kbd>pacman -S xf86-video-ati</kbd>
<em>--- output not shown here ---</em>
</pre>

### Video: GMA500 (Intel)

**SINX-450**

<pre class="console">
<strong>#</strong> <kbd>pacman -S xf86-video-fbdev</kbd>
<em>--- output not shown here ---</em>
</pre>

Add _gma500_gfx_ module to modules array on /etc/mkinitcpio.conf and build initramfs

<pre class="console">
<strong>#</strong> <kbd>mkinitcpio -p linux</kbd>
<em>--- output not shown here ---</em>
</pre>

### Video: Intel Graphics Card

**Acer V5-571PG, Acer 4738, HP/Compaq 6720s**

<pre class="console">
<strong>#</strong> <kbd>pacman -S xf86-video-intel</kbd>
<em>--- output not shown here ---</em>
</pre>

**Note:** for _Acer V5-571PG_ see also settings up [NVIDIA Optimus](#videonvidia-graphics-card)

Optionally, add _i915_ module to modules array on /etc/mkinitcpio.conf and
build initramfs

<pre class="console">
<strong>#</strong> <kbd>mkinitcpio -p linux</kbd>
<em>--- output not shown here ---</em>
</pre>

### Video:  NVIDIA Graphics Card

**Lenovo 3000 N100**

<pre class="console">
<strong>#</strong> <kbd>pacman -S xf86-video-nouveau nouveau-dri</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>cat > /etc/X11/xorg.conf.d/20-nouveau.conf << EOF</kbd>
<strong>></strong> <kbd>Section "Device"</kbd>
<strong>></strong> <kbd>  Identifier "Nvidia card"</kbd>
<strong>></strong> <kbd>  Driver "nouveau"</kbd>
<strong>></strong> <kbd>EndSection</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

**Acer V5-751PG (Optimus)**

<pre class="console">
<strong>#</strong> <kbd>yaourt -S xf86-video-intel intel-dri bumblebee nvidia-dkms</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>systemctl enable bumblebeed</kbd>
</pre>

### WIFI: Broadcom BCM4311

**IBM R52**

<pre class="console">
<strong>$</strong> <kbd>yaourt -S b43-firmware rfkill</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>systemctl enable rfkill-unblock\@wifi</kbd>
</pre>

_might have to add `RFKILL_UNBLOCK="wifi"` to /etc/conf.d/rfkill_

### WIFI: Broadcom BCM43228

**SINX-450**

<pre class="console">
<strong>$</strong> <kbd>yaourt -S broadcom-wl-dkms</kbd>
<em>--- output not shown here ---</em>
</pre>

## GUI

Clone the installation script GIT repository again into the newly installed
system, and run the final script.

<pre class="console">
<strong>#</strong> <kbd>./052-install-gui.sh &lt;de_name&gt;</kbd>
<em>--- output not shown here ---</em>
</pre>

Parameter _de_name_ should be either **i3** or **xfce**.


## Post install setup

  * Clone _bin_ and _vault_ repositories from the shared volume and run setup script.
  * Clone dotfiles repository from local server and run setup (while in console).
  * Reboot into GUI and run the dotfiles setup again to properly configure environment variables.
  * Do a final reboot.
