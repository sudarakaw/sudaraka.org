+++
title = "Make bootable USB from CD/DVD ISO image"
description = "Make bootable USB from CD/DVD ISO image"
date = 2012-11-25T07:28:55+05:30

[extra]
keywords = "boot,usb,cd,dvd,iso,linux,syslinux"
+++


* Create an image file with enough space to hold the content from ISO.
* Install syslinux on the image.
* Copy everything from ISO to the new image.
* Replace isolinux references with syslinux.
* Write the image to USB device.
<!-- more -->

<pre class="console">
<strong>$</strong> <kbd>du -Lh /path/to/image.iso | cut -d'M' -f1</kbd>
700
<strong>$</strong> <kbd>dd if=/dev/zero of=/tmp/usb-image bs=1M count=710</kbd>
710+0 records in
710+0 records out
744488960 bytes (744 MB) copied, 0.825134 s, 902 MB/s
<strong>$</strong> <kbd>mkfs.vfat /tmp/usb-image</kbd>
mkfs.vfat 3.0.12 (29 Oct 2011)
<strong>$</strong> <kbd>syslinux /tmp/usb-image</kbd>
<strong>$</strong> <kbd>mkdir /tmp/{usb,iso}</kbd>
<strong>#</strong> <kbd>mount -o loop /tmp/usb-image /tmp/usb</kbd>
<strong>#</strong> <kbd>mount -o loop /path/to/image.iso /tmp/iso</kbd>
mount: warning: /tmp/iso seems to be mounted read-only.
<strong>#</strong> <kbd>cp -r /tmp/iso/* /tmp/usb</kbd>
<strong>#</strong> <kbd>mv /tmp/usb/{iso,sys}linux</kbd>
<strong>#</strong> <kbd>mv -v /tmp/usb/syslinux/{iso,sys}linux.cfg</kbd>
<strong>#</strong> <kbd>sed -i 's/isolinux/syslinux/g' /tmp/usb/syslinux/*.cfg</kbd>
<strong>#</strong> <kbd>umount /tmp/{iso,usb}</kbd>
<strong>#</strong> <kbd>dd if=/tmp/usb-image of=/dev/sdX</kbd>
1454080+0 records in
1454080+0 records out
744488960 bytes (744 MB) copied, 286.461 s, 2.6 MB/s
<strong>#</strong> <kbd>eject /dev/sdX</kbd>
</pre>

Note:

  * isolinux directory maybe located in either root of the image or in the /boot sub-directory
