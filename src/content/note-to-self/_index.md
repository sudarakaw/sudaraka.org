+++
title = "Notes To Self"
description = "Articles with quick references and notes for my personal use."

sort_by = "date"

[extra]
keywords = "sudaraka,notes,reference"
+++


The category "Note To Self" contains quick & dirty articles for my personal
reference. However you are also free to use any content you find under this
category.
