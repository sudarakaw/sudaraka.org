+++
title = "CRUX REPO Script"
description = "Bash script that generates the REPO file for CRUX ports tree"
date = 2012-11-25T07:28:55+05:30

[extra]
keywords = "crux,ports,repo,script"
+++


Following bash script generates the REPO file for httpup ports tree.
<!-- more -->

**Usage:**

Place this script on the root of the ports tree directory (where REPO file
suppose to be) and execute it.

```bash
#!/bin/bash
#
# build-repo: Build the REPO file
# Copyright Sudaraka Wijesinghe <sudaraka dot wijesinghe at gmail dot com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#  USAGE: build-repo
#

[ -f REPO ] && rm REPO

for dir in `find . -type d -regex "\..+" -printf "%f\n"`; do
	if [[ -f $dir/Pkgfile ]]; then
		if [[ ! -z `grep -e "^# Group:" $dir/Pkgfile` || -f $dir/.md5sum ]]; then
			echo "d:$dir" >> REPO
		else
			echo "Make sure $dir contains .md5sum";
		fi
	else
		echo "Make sure $dir contains Pkgfile";
	fi
done

find . -type f -regex "\.\/.+\/.+" -exec md5sum {} \;|sed -e 's/\(.*\)\s\s\.\//f:\1:/' >> REPO
```
