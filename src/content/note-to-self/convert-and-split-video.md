+++
title = "Convert and split video"
description = "Convert and split video in Linux using ffmpeg"
date = 2012-11-24T00:34:36+05:30

[extra]
keywords = "linux,ffmpeg,video,convert,split,cli"
+++


<pre class="console">
  <strong>$</strong> <kbd>ffmpeg -i input.avi -s 1280x720 -vcodec mpeg2video -sameq -ss 00:00:00 -t 01:00:00 output.mpg</kbd>
</pre>

Note: above example is to convert AVI file to Sony Bravia KDL-32V5500 compatible MPEG2 format.
