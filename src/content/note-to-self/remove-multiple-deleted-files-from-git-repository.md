+++
title = "Remove multiple deleted files from GIT repository"
description = "Remove multiple deleted files from GIT repository"
date = 2012-08-12T03:13:07+05:30

[extra]
keywords = "git,remove,multiple,deleted,cli"
+++


<pre class="console"><strong>$</strong> <kbd>git status|grep deleted|cut -d' ' -f5|xargs git rm</kbd></pre>
