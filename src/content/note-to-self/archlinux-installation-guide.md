+++
title = "Arch Linux Installation Guide"
description = "An Arch Linux installation guide customized for my hardware and needs"
date = 2014-02-08T02:19:06+05:30

[extra]
keywords = "customized,archlinux,installation,guide,linux"
+++


This Arch Linux installation guide is customized for my hardware and my needs.
Anyone may use these instructions to setup a system of their own, but they
are not guaranteed to work on all hardware nor they are the "official" way of
doing things.
<!-- more -->

**Disclaimer:** software components, specially the ones that I block out are
my personal choice, and anyone else referencing this article should
substitute their own choices.

For a generic installations instructions see the official [Arch Linux Installation Guide](https://wiki.archlinux.org/index.php/Installation_Guide).

Installation done using **archlinux-2013.07.01-dual.iso**


## Preparing Partitions

Create the following partitions on disk(s) using fdisk.

**Single disk setup**

Partition  | Type | Size | Device Node<br>(assumed)
-|-|-:|-
/  | primary, ext4, +boot | 10GB | /dev/sda1
swap | logical, swap | 2GB | /dev/sda5
/home | logical, ext4 | remainder | /dev/sda6

**Dual disk setup**

Partition  | Type  |  Size | Device Node<br>(assumed)
-|-|-:|-
/  | primary, ext4, +boot  |  10GB | /dev/sda1
/home  | logical, ext4  |  remainder | /dev/sda5
 | | | | |
swap  | logical, swap  |  2GB | /dev/sdb5
/var/disk2  | logical, ext4  |  remainder | /dev/sdb6

Format each partition

<pre class="console">
<strong>#</strong> <kbd>mkswap /dev/sda5</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>mkfs.ext4 /dev/sda1</kbd>
<em>--- output not shown here ---</em>
</pre>

Note: make sure you format all ext4 partitions.

## Prepare for base installation

### Mount file system

Mount new partition to a temporary location (/mnt) and create root directory
structure.

<pre class="console">
<strong>#</strong> <kbd>swapon /dev/sda5</kbd>
<strong>#</strong> <kbd>mount /dev/sda1 /mnt</kbd>
<strong>#</strong> <kbd>mkdir -pv /mnt/{boot,dev,etc/wpa_supplicant,home,proc,root,run,sys,tmp,var/{cache/pacman/pkg,disk2,lib/pacman}}</kbd>
mkdir: created directory '/mnt/boot'
mkdir: created directory '/mnt/dev'
mkdir: created directory '/mnt/etc'
mkdir: created directory '/mnt/etc/wpa_supplicant'
mkdir: created directory '/mnt/home'
mkdir: created directory '/mnt/proc'
mkdir: created directory '/mnt/root'
mkdir: created directory '/mnt/run'
mkdir: created directory '/mnt/sys'
mkdir: created directory '/mnt/tmp'
mkdir: created directory '/mnt/var'
mkdir: created directory '/mnt/var/cache'
mkdir: created directory '/mnt/var/cache/pacman'
mkdir: created directory '/mnt/var/cache/pacman/pkg'
mkdir: created directory '/mnt/var/disk2'
mkdir: created directory '/mnt/var/lib'
mkdir: created directory '/mnt/var/lib/pacman'
</pre>

### Connect installation environment to WIFI/Internet

Enable WIFI connection using WPA2. When using a wired connection this steps
in this section may be skipped partly or completely.

<pre class="console">
<strong>#</strong> <kbd>wpa_passphrase ESSID uber_secret_key|sed '/#psk=/d' > /mnt/etc/wpa_supplicant/wifi.conf</kbd>
<strong>#</strong> <kbd>wpa_supplicant -B -D wext -i wlp1s0 -c /mnt/etc/wpa_supplicant/wifi.conf</kbd>
<strong>#</strong> <kbd>dhcpcd wlp1s0</kbd>
</pre>

### Modify pacman configuration

Block Mono platform from being installed in the system, thus block any software
components depends on them. Then include the archlinux.fr repository to enable
the [yaourt](https://wiki.archlinux.org/index.php/yaourt) installation.

<pre class="console">
<strong>#</strong> <kbd>sed "s/#\(IgnorePkg\s*=\)/\1 mono mono-tools jre7-openjdk-headless/" \</kbd>
<strong>></strong> <kbd>/etc/pacman.conf > /mnt/etc/pacman.conf</kbd>
<strong>#</strong> <kbd>cat >> /mnt/etc/pacman.conf << EOF</kbd>
<strong>></strong>
<strong>></strong> <kbd>[archlinuxfr]</kbd>
<strong>></strong> <kbd>Server = http://repo.archlinux.fr/\$arch</kbd>
<strong>></strong> <kbd>SigLevel = Optional</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

Mount cache server into /var/cache/pacman/pkg **of the installation
environment**.

<pre class="console">
<strong>#</strong> <kbd>mkdir -p /var/lib/pacman/sync</kbd>
<strong>#</strong> <kbd>mount -o nolock server_name:/path/to/sync /var/lib/pacman/sync</kbd>
<strong>#</strong> <kbd>mount -o nolock server_name:/path/to/cache /var/cache/pacman/pkg</kbd>
</pre>

For the installation you can use mirror closest to you and later do a
full system upgrade using an up-to-date repository. Select the closest
mirror and move it to the top of /etc/pacman.d/mirrorlist

## Install base system

**Note:** Only ignore _linux_ package when prepared to build a custom kernel.

<pre class="console">
<strong>#</strong> <kbd>pacman -Sy -r /mnt --config /mnt/etc/pacman.conf \</kbd>
<strong>></strong> <kbd>--ignore jfsutils,reiserfsprogs,xfsprogs,vi,nano,lvm2,netctl,linux,linux-firmware,heirloom-mailx,mdadm,pcmciautils\</kbd>
<strong>></strong> <kbd>base base-devel syslinux wireless_tools wpa_supplicant gvim sudo yaourt rsync wget git nfs-utils ntp bc</kbd>
</pre>

Installation will replace our _pacman.conf_ and we need to restore it

<pre class="console">
<strong>#</strong> <kbd>mv -v /mnt/etc/pacman.conf{,.pacnew}</kbd>
'/mnt/etc/pacman.conf' -> '/mnt/etc/pacman.conf.pacnew'
<strong>#</strong> <kbd>mv -v /mnt/etc/pacman.conf{.pacorig,}</kbd>
'/mnt/etc/pacman.conf.pacorig' -> '/mnt/etc/pacman.conf'
</pre>

## Pre-boot configuration

### Chroot into the new system

Mount dev, proc and sys to be used in chroot, also bind the /tmp into
chroot directory.

<pre class="console">
<strong>#</strong> <kbd>mount -t devtmpfs udev /mnt/dev</kbd>
<strong>#</strong> <kbd>mount -t proc proc /mnt/proc</kbd>
<strong>#</strong> <kbd>mount -t sysfs sys /mnt/sys</kbd>
<strong>#</strong> <kbd>mount --bind /tmp /mnt/tmp</kbd>
<strong>#</strong> <kbd>chroot /mnt /bin/bash</kbd>
</pre>

### /etc/fstab

For a single disk system.

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/fstab << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>/dev/sda1 / ext4 defaults,noatime,nodiratime,discard,errors=remount-ro 0 1</kbd>
<strong>></strong> <kbd>/dev/sda6 /home ext4 defaults,noatime,nodiratime,discard,errors=remount-ro 0 1</kbd>
<strong>></strong> <kbd>/dev/sda5 swap swap sw,noatime,nodiratime 0 0</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

For a dual disk system.

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/fstab << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>/dev/sda1 / ext4 defaults,noatime,nodiratime,discard,errors=remount-ro 0 1</kbd>
<strong>></strong> <kbd>/dev/sda5 /home ext4 defaults,noatime,nodiratime,discard,errors=remount-ro 0 1</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>/dev/sdb5 swap swap sw,noatime,nodiratime 0 0</kbd>
<strong>></strong> <kbd>/dev/sdb6 /var/disk2 ext4 noauto,x-systemd.automount,defaults,noatime,nodiratime,errors=remount-ro 0 2</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

Also make the pacman cache server directory mount on boot.

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/fstab << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>server_name:/path/to/cache /var/cache/pacman/pkg nfs noauto,x-systemd.automount,noexec,nolock,noatime,nodiratime,rsize=32768,wsize=32768,timeo=14,intr 0 0</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

### /etc/hostname and /etc/hosts

Machine name used here must also be entered into /etc/hosts as an alias to the
localhost.

<pre class="console">
<strong>#</strong> <kbd>echo 'machine-name' > /etc/hostname</kbd>
<strong>#</strong> <kbd>sed 's/\(localhost\)$/\1 machine-name/g' -i /etc/hosts</kbd>
</pre>

### Time zone and clock settings

Setting time zone to Asia/Colombo.

<pre class="console">
<strong>#</strong> <kbd>echo 'Asia/Colombo' > /etc/timezone</kbd>
<strong>#</strong> <kbd>ln -s /usr/share/zoneinfo/Asia/Colombo /etc/localtime</kbd>
<strong>#</strong> <kbd>hwclock --hctosys --utc</kbd>
<strong>#</strong> <kbd>date</kbd>
Sun Aug 26 02:06:34 IST 2012
</pre>

Set NTP service/daemon to start on boot, and point to the local NTP server.

<pre class="console">
<strong>#</strong> <kbd>ln -sv /usr/lib/systemd/system/ntpd.service /etc/systemd/system/multi-user.target.wants/</kbd>
'/etc/systemd/system/multi-user.target.wants/ntpd.service' -> '/usr/lib/systemd/system/ntpd.service'
<strong>#</strong> <kbd>sed 's/\(server\s\+.\+\)/#\1/' -i /etc/ntp.conf</kbd>
<strong>#</strong> <kbd>echo 'server server_name' > /etc/ntp.conf</kbd>
</pre>

Optionally, if the system time need to be changed:

<pre class="console">
<strong>#</strong> <kbd>date MMDDhhmmYYYY</kbd>
<strong>#</strong> <kbd>hwclock --systohc --utc</kbd>
<strong>#</strong> <kbd>date</kbd>
DDD MMM DD hh:mm:?? IST YYYY
</pre>

### Generate locale

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/locale.gen << EOF</kbd>
<strong>></strong> <kbd>en_US.UTF-8 UTF-8</kbd>
<strong>></strong> <kbd>en_US ISO-8859-1</kbd>
<strong>></strong> <kbd>si_LK.UTF-8 UTF-8</kbd>
<strong>></strong> <kbd>EOF</kbd>
<strong>#</strong> <kbd>locale-gen</kbd>
Generating locales...
  en_US.UTF-8 UTF-8... done
  en_US ISO-8859-1... done
  si_LK.UTF-8 UTF-8... done
Generation complete.
</pre>

### Install bootloader (syslinux)

<pre class="console">
<strong>#</strong> <kbd>rm -fr /boot/syslinux</kbd>
<strong>#</strong> <kbd>cat > /boot/syslinux.cfg << EOF</kbd>
<strong>></strong> <kbd>DEFAULT linux</kbd>
<strong>></strong> <kbd>PROMPT 0</kbd>
<strong>></strong> <kbd>TIMEOUT 60</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>LABEL linux</kbd>
<strong>></strong> <kbd>  MENU LABEL Arch Linux (Custom Kernel Configuration)</kbd>
<strong>></strong> <kbd>  LINUX kernel</kbd>
<strong>></strong> <kbd>  APPEND root=/dev/sda1 ro quiet</kbd>
<strong>></strong> <kbd>EOF</kbd>
<strong>#</strong> <kbd>extlinux -i /boot</kbd>
/boot/syslinux is device /dev/sda1
<strong>#</strong> <kbd>dd if=/usr/lib/syslinux/mbr.bin of=/dev/sda bs=440 count=1</kbd>
1+0 records in
1+0 records out
440 bytes (440 B) copied, 0.00345276 s, 127 kB/s
</pre>

### Kernel

If you have the pre-compiled kernel binaries, copy them in to respective
locations, otherwise compile and install the kernel.


### Systemd adjustments

#### Stop console clearing at login prompt

<pre class="console">
<strong>#</strong> <kbd>sed 's/\(TTYVTDisallocate=\)yes/\1no/' /usr/lib/systemd/system/getty\@.service \</kbd>
<strong>></strong> <kbd>> /etc/systemd/system/getty\@.service</kbd>
<strong>#</strong> <kbd>rm /etc/systemd/system/getty.target.wants/getty\@tty1.service</kbd>
<strong>#</strong> <kbd>ln -sv ../getty\@.service /etc/systemd/system/getty.target.wants/getty\@tty1.service</kbd>
'/etc/systemd/system/getty.target.wants/getty@tty1.service' -> '../getty@.service'
</pre>

#### Limit number of TTYs

<pre class="console">
<strong>#</strong> <kbd>sed 's/#\(NAutoVTs=\).\+/\12/' -i /etc/systemd/logind.conf</kbd>
</pre>

#### Limit journal disk usage

<pre class="console">
<strong>#</strong> <kbd>sed 's/#\(SystemMaxUse=\).*/\116M/' -i /etc/systemd/journald.conf</kbd>
</pre>

#### Make wireless connection on boot

_Type_ of the dhcpcd@.service is changed to _idle_ in order to avoid login
prompt pollution.

<pre class="console">
<strong>#</strong> <kbd>sed 's/\(Type=\).\+/\1idle/' /usr/lib/systemd/system/dhcpcd\@.service |\</kbd>
<strong>></strong> <kbd>sed 's/\(Before=.\+\)/\1 var-cache-pacman-pkg.mount/' |\</kbd>
<strong>></strong> <kbd>sed '/Before=/i After=wpa_supplicant@wlp1s0.service'</kbd>
<strong>></strong> <kbd>> /etc/systemd/system/dhcpcd\@.service</kbd>
<strong>#</strong> <kbd>ln -sv ../dhcpcd\@.service /etc/systemd/system/multi-user.target.wants/dhcpcd\@wlp1s0.service</kbd>
'/etc/systemd/system/multi-user.target.wants/dhcpcd@wlp1s0.service' -> '../dhcpcd@.service'
<strong>#</strong> <kbd>mkdir -pv /etc/systemd/system/dhcpcd\@wlp1s0.service.wants</kbd>
mkdir: created directory '/etc/systemd/system/dhcpcd@wlp1s0.service.wants'
<strong>#</strong> <kbd>ln -sv /usr/lib/systemd/system/wpa_supplicant\@.service \</kbd>
<strong>></strong> <kbd>/etc/systemd/system/dhcpcd\@wlp1s0.service.wants/wpa_supplicant\@wlp1s0.service</kbd>
'/etc/systemd/system/dhcpcd@wlp1s0.service.wants/wpa_supplicant@wlp1s0.service' -> '/usr/lib/systemd/system/wpa_supplicant@.service'
<strong>#</strong> <kbd>ln -sv wifi.conf /etc/wpa_supplicant/wpa_supplicant-wlp1s0.conf</kbd>
'/etc/wpa_supplicant/wpa_supplicant-wlp1s0.conf' -> 'wifi.conf'
</pre>

#### Unmount pacman cache before shutdown/reboot

Modify the

<pre class="console">
<strong>#</strong> <kbd>mkdir -pv /etc/systemd/system/var-cache-pacman-pkg.mount.wants</kbd>
mkdir: created directory '/etc/systemd/system/var-cache-pacman-pkg.mount.wants'
<strong>#</strong> <kbd>ln -sv ../dhcpcd\@.service /etc/systemd/system/var-cache-pacman-pkg.mount.wants/dhcpcd\@wlp1s0.service</kbd>
'/etc/systemd/system/var-cache-pacman-pkg.mount.wants/dhcpcd@wlp1s0.service' -> '../dhcpcd@.service'
</pre>

### Exit chroot, unmount and reboot

<pre class="console">
<strong>#</strong> <kbd>exit</kbd>
exit
<strong>#</strong> <kbd>umount /mnt/{dev,proc,sys,tmp,var,}</kbd>
<strong>#</strong> <kbd>umount /var/cache/pacman/pkg</kbd>
<strong>#</strong> <kbd>reboot</kbd>
</pre>

## First-boot configuration

### pacman mirror list

Get latest mirror list and enable required mirrors

<pre class="console">
<strong>#</strong> <kbd>wget http://www.archlinux.org/mirrorlist/?use_mirror_status=on -O /etc/pacman.d/mirrorlist</kbd>
</pre>

### Initialize package keys

In order to generate entropy during the key initialization, switch to another
console and run **ls -Ra / >/dev/null** or use [haveged](https://www.archlinux.org/packages/?name=haveged).

In order to install _haveged_ you may need to temporarily set `SigLevel` to
`TrustAll` for the `[community]` repository.

<pre class="console">
<strong>#</strong> <kbd>pacman-key --init</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>pacman-key --populate archlinux</kbd>
<em>--- output not shown here ---</em>
</pre>

Although it might nor be necessary for a net installation, do a system update.

<pre class="console">
<strong>#</strong> <kbd>yaourt -Syyua</kbd>
<em>--- output not shown here ---</em>
</pre>

### Create user(s)

<pre class="console">
<strong>#</strong> <kbd>useradd -m -s /bin/bash -G audio,network,power,scanner,storage,systemd-journal,video,wheel -U new_username</kbd>
<strong>#</strong> <kbd>passwd new_username</kbd>
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
</pre>

You may need to add **lp** and **optical** groups to if you are planing
to use those devices or applications.

Add this user to /etc/sudoers as we will be blocking root from login in
the system.

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/sudoers << EOF</kbd>
<strong>></strong> <kbd>new_user ALL=(ALL) ALL</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

Lockout root from login into console or from SSH in. Optionally remove
the password hash for root from /etc/shadow

<pre class="console">
<strong>#</strong> <kbd>rm /etc/securetty</kbd>
<strong>#</strong> <kbd>sed 's#\(root:x:0:0:root:/root:/bin/\).\+#\1false#' -i /etc/passwd</kbd>
<strong>#</strong> <kbd>sed 's/\(root:\)[^:]\+\(:.\+\)/\1x\2/' -i /etc/shadow</kbd>
</pre>

### Disable webcam

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/modprobe.d/modprobe.conf << EOF</kbd>
<strong>></strong> <kbd>blacklist uvcvideo</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

## Driver installation and configuration

### Sound: ALSA Support

ALSA support must be enabled in the Kernel. Following installs a set of
utilities for configuration and integration need that applies to all hardware.

<pre class="console">
<strong>#</strong> <kbd>pacman -S alsa-utils alsa-oss alsa-plugins</kbd>
<em>--- output not shown here ---</em>
</pre>

Do the necessary adjustments using **alsamixer** and test and save settings.

<pre class="console">
<strong>$</strong> <kbd>alsamixer</kbd>
<strong>$</strong> <kbd>speaker-test -c 2</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>alsactl store</kbd>
</pre>

Optionally, make the necessary changes to the configuration files to load
ALSA daemon on boot.

### Touch Pad: FSPPS/2 Seltelic

**SINX-450**

<pre class="console">
<strong>#</strong> <kbd>pacman -S xf86-input-synaptics</kbd>
<em>--- output not shown here ---</em>
</pre>

### Video: ATI Graphics Card

**IBM R52**

<pre class="console">
<strong>#</strong> <kbd>pacman -S xf86-video-ati</kbd>
<em>--- output not shown here ---</em>
</pre>

### Video: GMA500 (Intel)

**SINX-450**

<pre class="console">
<strong>#</strong> <kbd>pacman -S xf86-video-fbdev</kbd>
<em>--- output not shown here ---</em>
</pre>

Add _gma500_gfx_ module to modules array on /etc/mkinitcpio.conf and build initramfs

<pre class="console">
<strong>#</strong> <kbd>mkinitcpio -p linux</kbd>
<em>--- output not shown here ---</em>
</pre>

### Video: Intel Graphics Card

**Acer V5-571PG, Acer 4738, HP/Compaq 6720s**

<pre class="console">
<strong>#</strong> <kbd>pacman -S xf86-video-intel</kbd>
<em>--- output not shown here ---</em>
</pre>

**Note:** for _Acer V5-571PG_ see also settings up [NVIDIA Optimus](#videonvidia-graphics-card)

Optionally, add _i915_ module to modules array on /etc/mkinitcpio.conf and
build initramfs

<pre class="console">
<strong>#</strong> <kbd>mkinitcpio -p linux</kbd>
<em>--- output not shown here ---</em>
</pre>

### Video:  NVIDIA Graphics Card

**Lenovo 3000 N100**

<pre class="console">
<strong>#</strong> <kbd>pacman -S xf86-video-nouveau nouveau-dri</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>cat > /etc/X11/xorg.conf.d/20-nouveau.conf << EOF</kbd>
<strong>></strong> <kbd>Section "Device"</kbd>
<strong>></strong> <kbd>  Identifier "Nvidia card"</kbd>
<strong>></strong> <kbd>  Driver "nouveau"</kbd>
<strong>></strong> <kbd>EndSection</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

**Acer V5-751PG (Optimus)**

<pre class="console">
<strong>#</strong> <kbd>yaourt -S xf86-video-intel intel-dri bumblebee nvidia-dkms</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>systemctl enable bumblebeed</kbd>
</pre>

### WIFI: Broadcom BCM4311

**IBM R52**

<pre class="console">
<strong>$</strong> <kbd>yaourt -S b43-firmware rfkill</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>systemctl enable rfkill-unblock\@wifi</kbd>
</pre>

_might have to add `RFKILL_UNBLOCK="wifi"` to /etc/conf.d/rfkill_

### WIFI: Broadcom BCM43228

**SINX-450**

<pre class="console">
<strong>$</strong> <kbd>yaourt -S broadcom-wl-dkms</kbd>
<em>--- output not shown here ---</em>
</pre>

### WIFI: Realtek RTL8191SEvB

**SINX-450**
**Acer V5-571PG**

<pre class="console">
<strong>#</strong> <kbd>systemctl enable rfkill-unblock\@wifi</kbd>
</pre>

## Xorg

<pre class="console">
<strong>#</strong> <kbd>pacman -S xorg-server xorg-utils xorg-server-utils xorg-xinit mesa mesa-demos</kbd>
<em>--- output not shown here ---</em>
</pre>

Configure X to start on user login.

<pre class="console">
<strong>#</strong> <kbd>cp -v /etc/skel/.xinitrc ~</kbd>
'/etc/skel/.xinitrc' -> '/home/username/.xinitrc'
<strong>#</strong> <kbd>cat >> ~/.bash_profile << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>if [[ -z \$DISPLAY ]] && [[ /dev/tty1 = \$(tty) ]] && ! [[ -e /tmp/.X11-unix/X0 ]] && (( EUID )); then</kbd>
<strong>></strong> <kbd>  exec startx</kbd>
<strong>></strong> <kbd>fi</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

### Fonts

<pre class="console">
<strong>$</strong> <kbd>yaourt -S ttf-dejavu ttf-droid ttf-lklug</kbd>
<em>--- output not shown here ---</em>
</pre>

Following fonts may also be needed for a better UI experience.

  * ttf-bitstream-vera
  * ttf-cheapskate
  * ttf-freefont
  * ttf-inconsolata
  * ttf-indic-otf
  * ttf-junicode
  * ttf-liberation
  * ttf-linux-libertine


## XFCE

Install every member of the [xfce4](https://www.archlinux.org/packages/?name=xfce4) and _xfce4-goodies_ package
groups.

<pre class="console">
<strong>$</strong> <kbd>yaourt -S --ignore mousepad xfce4 xfce4-goodies xfce4-places-plugin gvfs</kbd>
<em>--- output not shown here ---</em>
</pre>

To start XFCE with the Xorg start up, modify the ~/.xinitrc as follows.

<pre class="console">
<strong>#</strong> <kbd>cat >> ~/.xinitrc << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>xbacklight -set 0 # 0 may turn off some monitor in some systems</kbd>
<strong>></strong> <kbd>xset -dpms    # Disable energy star features ?</kbd>
<strong>></strong> <kbd>xset s off    # Screen server off</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>exec startxfce4</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

### Theme & Icons: Numix Holo + elementary

Clone _Numix Holo_ themes and _elementary-xfce_ icon themes from github.
Also install dependencies gtk-engines-murrine and gtk-engines-unico

<pre class="console">
<strong>$</strong> <kbd>pacman -S gtk-engine-{unico,murrine}</kbd>
<em>--- output not shown here ---</em>
<strong>$</strong> <kbd>mkdir -pv ~/src</kbd>
mkdir: created directory '/home/suda/src'
<strong>$</strong> <kbd>cd ~/src</kbd>
<strong>$</strong> <kbd>git clone https://github.com/duskp/numix-holo.git</kbd>
<strong>$</strong> <kbd>git clone https://github.com/shimmerproject/elementary-xfce.git</kbd>
<strong>#</strong> <kbd>ln -s /home/suda/src/numix-holo /usr/share/themes</kbd>
<strong>#</strong> <kbd>ln -s /home/suda/src/elementary-xfce/elementary-xfce /usr/share/icons</kbd>
<strong>#</strong> <kbd>ln -s /home/suda/src/elementary-xfce/elementary-xfce-dark /usr/share/icons</kbd>
</pre>

## iBus

<pre class="console">
<strong>$</strong> <kbd>yaourt -S ibus</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>cat >> ~/.bashrc << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>export GTK_IM_MODULE=ibus</kbd>
<strong>></strong> <kbd>export XMODIFIERS=@im=ibus</kbd>
<strong>></strong> <kbd>export QT_IM_MODULE=ibus</kbd>
<strong>></strong> <kbd>export XIM_PROGRAM=/usr/bin/ibus-daemon</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

Install ibus-sayura (Sinhala input scheme) from [GIT](https://git.fedorahosted.org/cgit/ibus-sayura.git).

## Other Software

Software Application  | Repository Package  | AUR Package | Other Package
-|-|-|-
Chat & social media  | pidgin-libnotify  | | [hottot (GIT)](https://github.com/shellex/Hotot)
Cloud & file sharing tools | transmission-gtk  | dropbox, thunar-dropbox   | |
Encryption tools | truecrypt, keepassx  | | |
Email client  | thunderbird  | | |
Graphic tools  | gimp  | | |
Multimedia  | vlc  | | [gpodder (GIT)](https://github.com/gpodder/gpodder)
Network tools  | dnsutils  | | |
Office & productivity tools | libreoffice, gnucash, gnome-calculator, evince, aspell-en (Create ~/.vim/spell/en.utf-8.add symbolic link to ~/.aspell.en.pws so Vim share the aspell dictionary), hunspell-en, ispell  | | |
Remote access  | openssh, filezilla | | |
Software development tools | geany-plugins, meld, ghex | | |
Shell & file handling  | tree, dosfstools, ntfs-3g, file-roller, unrar, zip, unzip, p7zip, arj, guake  | | [screen (GIT)](http://git.savannah.gnu.org/cgit/screen.git)
Virtualization  | virtualbox | | |
Web browsers  | firefox, chromium | | |
Web development tools  | php-apache, mariadb-clients | | |

## Sample scripts

### ~/.bash_profile

```bash
#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [[ -z $DISPLAY ]] && [[ /dev/tty1 = $(tty) ]] && ! [[ -e /tmp/.X11-unix/X0 ]] && (( EUID )); then
  while true; do
    read -p 'Start X? (Y/n): '

    if [ -z "$REPLY" ]; then
      REPLY="Y"
    fi

    case $REPLY in
      [Yy]) printf '\n'; exec startx ;;
      [Nn]) break ;;
      *) printf '%s\n' 'Please answer y or n.' ;;
    esac
  done
fi
```

### ~/bin/startup.sh

To be used as a start up script for XFCE.

```bash
#!/bin/bash

(hotot &) &&
(sleep 3 && pidgin &) &&
(sleep 5 && guake)
```
