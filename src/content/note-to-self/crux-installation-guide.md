+++
title = "CRUX Installation Guide (incomplete)"
description = "A (incomplete) CRUX installation guide customized for my hardware and needs."
date = 2015-02-05T11:28:12+05:30

[extra]
keywords = "customized,crux,installation,guide,linux"
+++


_This article is incomplete_

This CRUX installation guide is customized for my hardware and my needs.
Anyone may use these instructions to setup a system of their own, but they
are not guaranteed to work on all hardware nor they are the "official" way of
doing things.
<!-- more -->

**Disclaimer:** software components, specially the ones that I block out are
my personal choice, and anyone else referencing this article should
substitute their own choices.

For a generic installations instructions see the official [CRUX Handbook](http://crux.nu/Main/Handbook2-8).

## Preparing Partitions

Create the following partitions on primary disk using fdisk.

Partition  | Type  |  Size | Device Node<br>(assumed)
-|-|-:|-
/  | primary, ext4, +boot  |  20GB | /dev/sda1
swap  | logical, swap  |  2GB | /dev/sda5
/home  | logical, ext4  |  remainder | /dev/sda6

Format each partition

<pre class="console">
<strong>#</strong> <kbd>mkswap /dev/sda5</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>mkfs.ext4 /dev/sda1</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>mkfs.ext4 /dev/sda6</kbd>
<em>--- output not shown here ---</em>
</pre>


## Prepare for base installation

### Mount file system

Mount new partition to a temporary location (/mnt) and install base packages.

<pre class="console">
<strong>#</strong> <kbd>swapon /dev/sda5</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>mount /dev/sda1 /mnt</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>setup</kbd>
<em>--- output not shown here ---</em>
</pre>

### Packages to install

Select the packages to install with the following guidelines. (Installation
program will automatically calculate dependencies.)

code<br>(All except) | opt<br>(Include) | xorg<br>(Include)
-|-|-
btrfs-progs  | alsa-lib  | mesa3d
ed  | fakeroot  | xorg
exim  | gtk  | xorg-font-*
jfutils  | hicolor-icon-theme | xorg-libxxf86misc
lilo  | openbox  | xorg-xbacklight
reiserfsprogs  | syslinux  | xorg-xf86-input-{keyboard,mouse,synaptics}
xfsprogs  | wireless-tools  | xorg-xf86-video-? (see [drivers](#driver-installation-and-configuration))
&nbsp; | wpa_supplicant  | xorg-xrandr
&nbsp; |  | xorg-xset
&nbsp; |  | xorg-xrdb
&nbsp; |  | libepoxy

## Pre-boot configuration

### Chroot into the new system

Use the provided setup-chroot script

<pre class="console">
<strong>#</strong> <kbd>setup-chroot</kbd>
</pre>

or, manually

<pre class="console">
<strong>#</strong> <kbd>mount --bind /dev /mnt/dev</kbd>
<strong>#</strong> <kbd>mount --bind /tmp /mnt/tmp</kbd>
<strong>#</strong> <kbd>mount -t proc proc /mnt/proc</kbd>
<strong>#</strong> <kbd>mount -t sysfs none /mnt/sys</kbd>
<strong>#</strong> <kbd>mount -t devpts devpts /mnt/dev/pts</kbd>
<strong>#</strong> <kbd>chroot /mnt /bin/bash</kbd>
</pre>

### /etc/fstab

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/fstab << EOF</kbd>
<strong>></strong> <kbd>/dev/sda1 / ext4 defaults,noatime,nodiratime,discard,errors=remount-ro 0 1</kbd>
<strong>></strong> <kbd>/dev/sda5 swap swap sw,noatime,nodiratime 0 0</kbd>
<strong>></strong> <kbd>/dev/sda6 /home ext4 defaults,noatime,nodiratime,discard,errors=remount-ro 0 1</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

### /etc/rc.conf

```ini
FONT=lat9w-16
KEYMAP=us
TIMEZONE=Asia/Colombo
HOSTNAME=sw-crux
SYSLOG=sysklogd
SERVICES=(crond)
```

### Generating locales

<pre class="console">
<strong>#</strong> <kbd>localedef -i en_US -f ISO-8859-1 en_US</kbd>
<strong>#</strong> <kbd>localedef -i en_US -f ISO-8859-1 en_US.ISO-8859-1</kbd>
<strong>#</strong> <kbd>localedef -i en_US -f UTF-8 en_US.UTF-8</kbd>
<strong>#</strong> <kbd>localedef -i si_LK -f UTF-8 si_LK.UTF-8</kbd>
</pre>

### Install Kernel

Compile the latest stable kernel from source/git, or copy the pre-compiled
binaries.


### Install bootloader (syslinux)

At this point you should have the kernel installed.

<pre class="console">
<strong>#</strong> <kbd>cat > /boot/syslinux/syslinux.cfg << EOF</kbd>
<strong>></strong> <kbd>DEFAULT crux</kbd>
<strong>></strong> <kbd>PROMPT 0</kbd>
<strong>></strong> <kbd>TIMEOUT 50</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>LABEL crux</kbd>
<strong>></strong> <kbd>  MENU LABEL CRUX</kbd>
<strong>></strong> <kbd>  LINUX ../vmlinuz</kbd>
<strong>></strong> <kbd>  APPEND root=/dev/sda1 ro</kbd>
<strong>></strong> <kbd>EOF</kbd>
<strong>#</strong> <kbd>extlinux -i /boot/syslinux</kbd>
/boot/syslinux is device /dev/sda1
<strong>#</strong> <kbd>dd if=/usr/share/syslinux/mbr.bin of=/dev/sda bs=440 count=1</kbd>
1+0 records in
1+0 records out
440 bytes (440 B) copied, 0.00345276 s, 127 kB/s
</pre>

### Set root password

<pre class="console">
<strong>#</strong> <kbd>passwd</kbd>
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
</pre>

### Create user(s)

Make sure you have the /home mounted

<pre class="console">
<strong>#</strong> <kbd>useradd -m -s /bin/bash -G audio,lp,video,wheel,scanner -U new_username</kbd>
<strong>#</strong> <kbd>passwd new_username</kbd>
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
</pre>

Add the _new_username_ to /etc/sudoers

<pre class="console">
<strong>#</strong> <kbd>cat >> /etc/sudoers << EOF</kbd>
<strong>></strong> <kbd>new_username ALL=(ALL) ALL</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

Lockout root from login into console or from SSH in.

<pre class="console">
<strong>#</strong> <kbd>rm /etc/securetty</kbd>
<strong>#</strong> <kbd>sed 's#root:x:0:0:root:/root:/bin/bash#root:x:0:0:root:/root:/bin/false#' -i /etc/passwd</kbd>
</pre>

### Exit chroot, unmount and reboot

Add _--noclear_ flag to the tty1 in /etc/inittab and remove additional ttys.

<pre class="console">
<strong>#</strong> <kbd>exit</kbd>
exit
<strong>#</strong> <kbd>umount /mnt/{dev{/pts,},tmp,proc,sys,home,}</kbd>
<strong>#</strong> <kbd>reboot</kbd>
</pre>

## First-boot configuration

### Connect to internet via WIFI

Enable WIFI connection using WPA2. When using a wired connection this steps
in this section may be skipped partly or completely.

Check the [drivers](#driver-installation-and-configuration) section to see
if you need any firmware installed.

<pre class="console">
<strong>#</strong> <kbd>wpa_passphrase ESSID uber_secret_key|sed '/#psk=/d' > /etc/wifi.conf</kbd>
<strong>#</strong> <kbd>wpa_supplicant -B -D wext -i wlan0 -c /etc/wifi.conf</kbd>
<strong>#</strong> <kbd>dhcpcd wlan0</kbd>
</pre>

Edit /etc/rc.local to start the internet connection on boot

### Configure build environment

Create a new user for building packages and create relevant sub-directories
in it's home directory. Also link the /usr/ports to our new location.

<pre class="console">
<strong>#</strong> <kbd>useradd -m -s /bin/false -U pkgmk</kbd>
<strong>#</strong> <kbd>mkdir -pv /home/pkgmk/{src,pkg,work,ports}</kbd>
mkdir: created directory '/home/pkgmk/src'
mkdir: created directory '/home/pkgmk/pkg'
mkdir: created directory '/home/pkgmk/work'
mkdir: created directory '/home/pkgmk/ports'
<strong>#</strong> <kbd>rmdir /usr/ports</kbd>
<strong>#</strong> <kbd>ln -s /home/pkgmk/ports /usr/ports</kbd>
<strong>#</strong> <kbd>chown -R pkgmk.users /home/pkgmk</kbd>
</pre>

Mount a ram disk to /home/pkgmk/work to make the compilation faster.

<pre class="console">
<strong>#</strong> <kbd>id pkgmk</kbd>
uid=101(pkgmk) gid=100(users) groups=100(users)
<strong>#</strong> <kbd>cat >> /etc/fstab << EOF</kbd>
<strong>></strong> <kbd>pkgmk /home/pkgmk/work tmpfs size=1G,uid=101,defaults,noatime,nodiratime 0 0</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

Enable _contrib_ repository

<pre class="console">
<strong>#</strong> <kbd>mv -v /etc/ports/contrib.rsync{.inactive,}</kbd>
'/etc/ports/contrib.rsync.inactive' -> '/etc/ports/contrib.rsync'
</pre>

#### /etc/prt-get.conf

<pre class="console">
<strong>#</strong> <kbd>sed 's/# \(makecommand\)/\1 sudo -H -u pkgmk fakeroot/' -i /etc/prt-get.conf</kbd>
<strong>#</strong> <kbd>cat >> /etc/prt-get.conf << EOF</kbd>
<strong>></strong> <kbd></kbd>
<strong>></strong> <kbd>prtdir /usr/ports/contrib</kbd>
<strong>></strong> <kbd>runscripts yes</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

#### /etc/pkgmk.conf

Remove _-pipe_ flags.

<pre class="console">
<strong>#</strong> <kbd>sed 's/ -pipe//g' -i /etc/pkgmk.conf</kbd>
</pre>

And make following changes

```ini
export MAKEFLAGS="-j$(getconf _NPROCESSORS_ONLN)"

PKGMK_SOURCE_DIR="/home/pkgmk/src"
PKGMK_PACKAGE_DIR="/home/pkgmk/pkg"
PKGMK_WORK_DIR="/home/pkgmk/work/$name"
PKGMK_IGNORE_FOOTPRINT="yes"
PKGMK_COMPRESSION_MODE="xz"

```

#### Update system

<pre class="console">
<strong>#</strong> <kbd>ports -u</kbd>
<strong>#</strong> <kbd>prt-get sysup</kbd>
</pre>

## Local ports tree

Add the _rso.httpup_ file for the repository to /etc/ports directory, and update
the ports tree

<pre class="console">
<strong>#</strong> <kbd>sudo wget http://repo.sudaraka.org/crux-ports/rso.httpup -O /etc/ports/rso.httpup</kbd>
<em>--- output not shown here ---</em>
<strong>#</strong> <kbd>sudo ports -u rso</kbd>
<em>--- output not shown here ---</em>
</pre>

Add the new ports directory to the /etc/prt-get.conf. Place it above other
port directories can override them

Install the required packages.

## Driver installation and configuration

### Sound: ALSA Support

ALSA support must be enabled in the Kernel. Following installs a set of
utilities for configuration and integration need that applies to all hardware.

<pre class="console">
<strong>#</strong> <kbd>prt-get depinst alsa-utils</kbd>
<em>--- output not shown here ---</em>
</pre>

Do the necessary adjustments using **alsamixer** and test and save settings.

<pre class="console">
<strong>$</strong> <kbd>alsamixer</kbd>
<strong>$</strong> <kbd>speaker-test -c 2</kbd>
<em>--- output not shown here ---</em>
</pre>

Optionally, make the necessary changes to the configuration files to load
ALSA daemon on boot.

### Video: GMA500 (Intel)

**SINX-450**

<pre class="console">
<strong>#</strong> <kbd>prt-get depinst xorg-xf86-video-fbdev</kbd>
<em>--- output not shown here ---</em>
</pre>

### Wifi: Realtek RTL8191SEvB

**SINX-450**

Install the binary firmware ([rtlwifi/rtl8192sefw.bin](https://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=tree;f=rtlwifi;h=5cce1fa2f49cb73106f1e944738b9899aad95945;hb=HEAD))
from kernel repository.

## Desktop environment (XFCE)

To enable XFCE ports, create the .rsync file manually.

<pre class="console">
<strong>#</strong> <kbd>cat > /etc/ports/xfce.rsync << EOF</kbd>
<strong>></strong> <kbd>host=crux.nu</kbd>
<strong>></strong> <kbd>collection=ports/crux-2.8/xfce/</kbd>
<strong>></strong> <kbd>destination=/usr/ports/xfce</kbd>
<strong>></strong> <kbd>EOF</kbd>
</pre>

Then modify the /etc/prt-get.conf to include the new xfce ports directory and
update ports tree.

Install the following packages:

<pre class="console">
<strong>#</strong> <kbd>prt-get -if depinst </kbd>...list from below...
</pre>

XFCE  | Support Applications  | Themes  | Dependencies
-|-|-|-
xfce4  | thunar-volman  | xfce-theme-greybird *  |gperf
xfce4-appfinder  | thunar-archive-plugin  | faenza-icon-theme *  | |
xfce4-mixer  | thunar-media-tags-plugin  | xorg-font-droid-ttf * | |
xfce4-power-manager  | orage | | |
xfce4-artwork  | ristretto | | |
xfce4-battery-plugin  | xfburn | | |
xfce4-netload-plugin | | | |
xfce4-notes-plugin | | | |
xfce4-screenshooter | | | |
xfce4-taskmanager | | | |
libxfcegui4 | | | |
xfce4-notifyd * | | | |
xfce4-places-plugin | | | |

\* _installed from the local repository._

## Other Software

  * ca-certificates
  * geany
  * git
  * guake
