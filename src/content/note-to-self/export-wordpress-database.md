+++
title = "Export Wordpress database"
description = "Export Wordpress database from local MySQL server to be imported into another."
date = 2012-09-28T15:40:38+05:30

[extra]
keywords = "mysql,database,wordpress,cli,export,import"
+++


Originally created for exporting database of a Wordpress site from
development server to production server, however this can be use for any
schema.
<!-- more -->

## Exporting

<pre class="console"><strong>$</strong> <kbd>mysqldump -Cnp schema_name|sed 's/local-domain/new-domain/g'|gzip &gt;schema.sql.gz</kbd></pre>

Note: generated script does not create or specify the destination database to
use. Intended to be imported in to an empty database.

## Importing

<pre class="console"><strong>$</strong> <kbd>echo "drop database if exists \`new_schema_name\`; create database \`new_schema_name\`; use \`new_schema_name\`; `zcat schema.sql.gz`"| \</kbd>
<strong>&gt;</strong> <kbd>mysql -p</kbd></pre>

or

<pre class="console"><strong>$</strong> <kbd>echo "drop database if exists \`new_schema_name\`; create database \`new_schema_name\`; use \`new_schema_name\`; `gzip -dc schema.sql.gz`"| \</kbd>
<strong>&gt;</strong> <kbd>mysql -p</kbd></pre>
