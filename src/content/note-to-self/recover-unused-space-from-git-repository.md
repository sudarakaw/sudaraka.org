+++
title = "Recover unused space from GIT repository"
description = "Recover unused space from GIT repository"
date = 2013-10-27T14:16:50+05:30

[extra]
keywords = "git,recover,diskspace,cli"
+++


<pre class="console">
<strong>$</strong> <kbd>git reflog expire --all --expire=now</kbd>
<strong>$</strong> <kbd>git gc --prune=now</kbd>
</pre>

Source: [Reclaim space from unused Git repository objects](http://jnrbsn.com/2011/03/reclaim-space-from-unused-git-repository-objects)
by [Jonathan Robson](http://jnrbsn.com/about.html)
