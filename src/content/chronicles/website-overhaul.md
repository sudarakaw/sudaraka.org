+++
title = "Website Overhaul"
date = 2012-08-12T01:48:00+05:30
description = "Website overhaul - complete overhaul of my web site within just month of the initial launch."

[extra]
keywords = "sudaraka.org,overhaul"
+++


Demonstrating yet another wiredness of my self, I have completely overhauled
my web site just within a month of the initial launch.
<!-- more -->

First version was done using [Wordpress](http://wordpress.org/) and I felt
it was over kill and boring for a person such as my self. So I decided to
KISS it. I must mention that I have nothing against [Wordpress](http://wordpress.org/),
it plays a main part in putting food on my plate, but for this job it was not
the right tool.

Main concern was because I use this web site as a place to keep my personal
notes and references, I frequently need to access it event when I'm offline
or on the road (yah, ok so I'm almost never on the road). Another reason that
[Wordpress](http://wordpress.org/) didn't work for me is because I'm not a
"blogger", I don't want the content of my web site to get lost over time,
therefor I decided the correct solution for me was a Wiki. [DokuWiki](https://www.dokuwiki.org/)
to be exact.

[DokuWiki](https://www.dokuwiki.org/) is a Wiki engine that keeps it's
content in plain text files, and allows us to edit them from out side of it
self. Coding of the [DokuWiki](https://www.dokuwiki.org/) was pretty simple
to understand and do the modifications, and has fairly nice documentation.

So, I took [DokuWiki](https://www.dokuwiki.org/) and striped it down
removing parts I don't want to use, and here we are!.

_I have explained the details of how my web site work in the article
[How Sudaraka.Org Works](@/chronicles/how-sudaraka-org-works.md)_
