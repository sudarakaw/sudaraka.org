+++
title = "Achievements"
description = "Certifications, Endorsements and other Achievements received by Sudaraka Wijesinghe"

[extra]
keywords = "sudaraka wijesinghe,achievement,certification,coursera,udemy,experts exchange,mongodb university"
css = [ "achievements" ]
+++

Following is a list of various certifications, endorsements and other
achievements received by [Sudaraka Wijesinghe](/about-sudaraka-wijesinghe).

## Coursera

<div class="certs coursera-certs">

[![Programming Languages, Part C](/assets/img/achievements/coursera/coursera-pl-c-201911.png)](https://www.coursera.org/account/accomplishments/verify/F8KCMYGFWY7A)
[Programming Languages, Part C](/assets/img/achievements/coursera/coursera-pl-c-201911.pdf)

[![Programming Languages, Part B](/assets/img/achievements/coursera/coursera-pl-b-201911.png)](https://www.coursera.org/account/accomplishments/verify/TBBDNQMHU2TW)
[Programming Languages, Part B](/assets/img/achievements/coursera/coursera-pl-b-201911.pdf)

[![Programming Languages, Part A](/assets/img/achievements/coursera/coursera-pl-a-201910.png)](https://www.coursera.org/account/accomplishments/verify/7R98USRMQGWH)
[Programming Languages, Part A](/assets/img/achievements/coursera/coursera-pl-a-201910.pdf)

[![Internet History, Technology, and Security, 30th December 2013](/assets/img/achievements/coursera/coursera-ihts-201310.png)](/assets/img/achievements/coursera/coursera-ihts-201310.pdf)
Internet History, Technology, and Security

[![Cryptography I, 10th November 2013](/assets/img/achievements/coursera/coursera-crypto-1-201309.png)](/assets/img/achievements/coursera/coursera-crypto-1-201309.pdf)
Cryptography I

[![Learn to Program: The Fundamentals, 20th October 2013](/assets/img/achievements/coursera/coursera-ltp-201308.png)](/assets/img/achievements/coursera/coursera-ltp-201308.pdf)
Learn to Program: The Fundamentals

[![Startup Engineering, 23rd September 2013](/assets/img/achievements/coursera/coursera-startup-201306.png)](/assets/img/achievements/coursera/coursera-startup-201306.pdf)
Startup Engineering

[![An Introduction to Interactive Programming in Python, 21st June 2013](/assets/img/achievements/coursera/coursera-python-201304.png)](/assets/img/achievements/coursera/coursera-python-201304.pdf)
An Introduction to Interactive Programming in Python

</div>

&nbsp;

## Udemy

<div class="certs udemy-certs">

[![Progressive Web Apps - The Concise PWA Masterclass](/assets/img/achievements/udemy/uc-kcgpfmxr.jpg)](https://www.udemy.com/certificate/UC-KCGPFMXR/)
Progressive Web Apps - The Concise PWA Masterclass

[![Web Design for Beginners: Real World Coding in HTML & CSS](/assets/img/achievements/udemy/uc-alq2a0kb.jpg)](https://www.udemy.com/certificate/UC-ALQ2A0KB/)
Web Design for Beginners: Real World Coding in HTML & CSS

[![Become a WordPress Developer: Unlocking Power With Code](/assets/img/achievements/udemy/uc-r7ccybmm.jpg)](https://www.udemy.com/certificate/UC-R7CCYBMM/)
Become a WordPress Developer: Unlocking Power With Code

[![Beginner's Guide to Elm Programming](/assets/img/achievements/udemy/uc-r1ydjycg.jpg)](https://www.udemy.com/certificate/UC-R1YDJYCG/)
Beginner's Guide to Elm Programming

[![The Rust Programming Language](/assets/img/achievements/udemy/uc-13e6kp8k.jpg)](https://www.udemy.com/certificate/UC-13E6KP8K/)
The Rust Programming Language

[![Reactive JS: Are you ready for the next big paradigm shift?](/assets/img/achievements/udemy/uc-gm7j1n0m.jpg)](https://www.udemy.com/certificate/UC-GM7J1N0M/)
Reactive JS: Are you ready for the next big paradigm shift?

[![Learn NoSQL Database Design With CouchDB](/assets/img/achievements/udemy/uc-owcgug70.jpg)](https://www.udemy.com/certificate/UC-OWCGUG70/)
Learn NoSQL Database Design With CouchDB

[![Modern React with Redux](/assets/img/achievements/udemy/uc-lt3en1hl.jpg)](https://www.udemy.com/certificate/UC-LT3EN1HL/)
Modern React with Redux

[![Building Better APIs with GraphQL](/assets/img/achievements/udemy/uc-d1renhj1.jpg)](https://www.udemy.com/certificate/UC-D1RENHJ1/)
Building Better APIs with GraphQL

[![Mastering React JS](/assets/img/achievements/udemy/uc-bj9i5pi1.jpg)](https://www.udemy.com/certificate/UC-BJ9I5PI1/)
Mastering React JS

[![Learn Nodejs by building 10 projects](/assets/img/achievements/udemy/uc-ddfkooju.jpg)](https://www.udemy.com/certificate/UC-DDFKOOJU/)
Learn Nodejs by building 10 projects

[![JavaScript: Understanding the Weird Parts on 14th April 2015](/assets/img/achievements/udemy/uc-3uhbpx5l.jpg)](https://www.udemy.com/certificate/UC-3UHBPX5L/)
JavaScript: Understanding the Weird Parts

</div>

## MongoDB University

<div class="certs mongo-certs">

[![MongoDB for Developers, 25th September 2015](/assets/img/achievements/mongouc/m101p-201509.png)](/assets/img/achievements/mongouc/M101P.pdf)
MongoDB for Developers

[![MongoDB for Node.js Developers, 25th September 2015](/assets/img/achievements/mongouc/m101js-201509.png)](/assets/img/achievements/mongouc/M101JS.pdf)
MongoDB for Node.js Developers

</div>

## Experts Exchange

<div class="certs ee-certs">

[![JavaScript - Guru](/assets/img/achievements/experts-exchange/javascript-guru-small.png)](#popup-1)
<span id="close-1"></span><div class="popup-overlay" id="popup-1">
[![JavaScript - Guru](/assets/img/achievements/experts-exchange/javascript-guru.png)](#close-1)
</div>

[![PHP - Guru](/assets/img/achievements/experts-exchange/php-guru-small.png)](#popup-2)
<span id="close-2"></span><div class="popup-overlay" id="popup-2">
[![PHP - Guru](/assets/img/achievements/experts-exchange/php-guru.png)](#close-1)
</div>

[![CSS - Master](/assets/img/achievements/experts-exchange/css-master-small.png)](#popup-3)
<span id="close-3"></span><div class="popup-overlay" id="popup-3">
[![CSS - Master](/assets/img/achievements/experts-exchange/css-master.png)](#close-3)
</div>

[![HTML - Master](/assets/img/achievements/experts-exchange/html-master-small.png)](#popup-4)
<span id="close-4"></span><div class="popup-overlay" id="popup-4">
[![HTML - Master](/assets/img/achievements/experts-exchange/html-master.png)](#close-4)
</div>

[![MySQL - Master](/assets/img/achievements/experts-exchange/mysql-master-small.png)](#popup-5)
<span id="close-5"></span><div class="popup-overlay" id="popup-5">
[![MySQL - Master](/assets/img/achievements/experts-exchange/mysql-master.png)](#close-5)
</div>

[![AJAX - Master](/assets/img/achievements/experts-exchange/ajax-master-small.png)](#popup-6)
<span id="close-6"></span><div class="popup-overlay" id="popup-6">
[![AJAX - Master](/assets/img/achievements/experts-exchange/ajax-master.png)](#close-6)
</div>

[![Wordpress - Master](/assets/img/achievements/experts-exchange/wordpress-master-small.png)](#popup-7)
<span id="close-7"></span><div class="popup-overlay" id="popup-7">
[![Wordpress - Master](/assets/img/achievements/experts-exchange/wordpress-master.png)](#close-7)
</div>


</div>

[download all](/assets/img/achievements/experts-exchange/certificates-all.pdf)

