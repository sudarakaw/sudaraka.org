
[ ...document.querySelectorAll('body > main a[href^=http]').values() ]
  .filter(a => !a.href.match(location.origin))
  .forEach(a => {
    a.setAttribute('target', '_new')
    a.classList.add('external')
  })
