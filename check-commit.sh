#!/usr/bin/env sh
# check-commit.sh: validate commit before applying it to the repository
#
# Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY;
# This is free software, and you are welcome to redistribute it and/or modify it
# under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

BUILD_MODIFIED=$(git diff --cached --name-only -- webroot/)
OTHER_MODIFIED=$(git diff --cached --name-only | grep -v '^webroot/')
BUILD_TAG=$(grep -P '^build:\s+' "$1")

if [ -n "$BUILD_MODIFIED" ] && [ -n "$OTHER_MODIFIED" ]; then
  # shellcheck disable=SC2039
  echo -e "\e[38;5;160mDo not mix webroot content with others in the same commit.\e[0m"

  exit 1
fi

if [ -n "$BUILD_MODIFIED" ] && [ -z "$BUILD_TAG" ]; then
  # shellcheck disable=SC2039
  echo -e "\e[38;5;160mCommit modifying webroot missing \"\e[1mbuild: \e[0;38;5;160m\" tag in message.\e[0m"

  exit 1
fi

exit 0
