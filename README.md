# Sudaraka.Org

Website template and content for [sudaraka.org](https://sudaraka.org/), built using [Zola](https://www.getzola.org/).

## License

Copyright 2018 Sudaraka Wijesinghe <sudaraka@sudaraka.org>

## Build scripts, template & code samples included in website

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.

## Website's content (excluding code samples)

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
